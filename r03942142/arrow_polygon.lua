local M = {}

function M:new()
	--ploygon
	local polygonW = 60
	local polygonH = 60
	local paint = { 0,0,0 }
	local vertices = { -polygonW/2,polygonH/2, polygonW/2,0, -polygonW/2,-polygonH/2}
	local poly = display.newPolygon( display.contentWidth*0.9, display.contentHeight*0.5, vertices )
		poly.fill = paint
		
		vertices = { polygonW/2,polygonH/2, -polygonW/2,0, polygonW/2,-polygonH/2}
	local poly2 = display.newPolygon( display.contentWidth*0.1, display.contentHeight*0.5, vertices )
		poly2.fill = paint
	--add to group
		poly = display.newGroup()
		poly2 = display.newGroup()
end

return M 