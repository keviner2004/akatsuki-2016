local M = {}

function M:new(score)
	--ploygon
	local polygonW = 80
	local polygonH = 160
	local level_room = 20
	local fading_time = 1000
	local left_down_x=polygonW/2
	local down_y=-polygonH/2
	local right_down_x=-polygonW/2
	local alpha = 1.0
	local delay = 1000
	local tree_color = { 165/256,42/256,42/256 }
	local vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2, left_down_x,down_y, right_down_x,down_y }
	--generate tree
	--set 5 level
	local tree_body_x = display.contentWidth*0.5
	local tree_body_y = display.contentHeight*0.75
	local tree_body
	--for level = 1,2, 1 do
		tree_body = display.newPolygon( tree_body_x, tree_body_y,vertices )
		tree_body.fill = tree_color
		tree_body_y = tree_body_y - display.contentHeight*0.1
		polygonH = polygonH*1.5
		vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2,left_down_x,down_y, right_down_x,down_y }
		--fade animation
		--local fadeTransition = transition.to( tree_body, { time = fading_time, alpha = 0.0 ,
			--[[onComplete=
				function( tree_body ) 
					transition.to( tree_body, {time = fading_time, alpha = 1.0} )
					timer.performWithDelay( 1000, listener )
					print(onComplete)
				end
		})
				--transition.fadeOut( tree_body, { fading_time, delay } )
	end--]]
	--end
	--generate tree point
	--set 20 level 
	--[[for level = 1,score/level_room, level_room do
		local circle = display.newCircle( display.contentWidth*0.5, display.contentHeight*0.3, 25 )
	end
	local simpleTransition = transition.to( circle, { time=1000, x=250, y=150 } )
	local polygonW = 60
	local polygonH = 60
	local paint = { 0,0,0 }
	local vertices = { -polygonW/2,polygonH/2, polygonW/2,0, -polygonW/2,-polygonH/2}
	local poly = display.newPolygon( display.contentWidth*0.9, display.contentHeight*0.5, vertices )
		poly.fill = paint
		
		vertices = { polygonW/2,polygonH/2, -polygonW/2,0, polygonW/2,-polygonH/2}
	local poly2 = display.newPolygon( display.contentWidth*0.1, display.contentHeight*0.5, vertices )
		poly2.fill = paint
	--add to group
		poly = display.newGroup()
		poly2 = display.newGroup()]]--
		
	function M:createTree(level)
		print("create tree")
		local tree_body = nil
			polygonW = 80
			polygonH = 160
			local tree_body_x = display.contentWidth*0.5
			local tree_body_y = display.contentHeight*0.75
			left_down_x = polygonW/2
			down_y = -polygonH/2
			right_down_x = -polygonW/2
			vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2, left_down_x,down_y, right_down_x,down_y }
		if level == 1 then
			--do nothing
		elseif level == 2 then
			tree_body_y = tree_body_y - display.contentHeight*0.1
			polygonH = polygonH*1.5
			vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2,left_down_x,down_y, right_down_x,down_y }
		elseif level == 3 then
			tree_body_y = tree_body_y - display.contentHeight*0.2
			polygonH = polygonH*math(1.5,2)
			vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2,left_down_x,down_y, right_down_x,down_y }
		elseif level == 4 then
			tree_body_y = tree_body_y - display.contentHeight*0.3
			polygonH = polygonH*math(1.5,3)
			vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2,left_down_x,down_y, right_down_x,down_y }
		else
			tree_body_y = tree_body_y - display.contentHeight*0.4
			polygonH = polygonH*math(1.5,4)
			vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2,left_down_x,down_y, right_down_x,down_y }
			--show tree
			tree_color = { 165/256,42/256,42/256 }
		tree_body = display.newPolygon( tree_body_x, tree_body_y,vertices )
		tree_body.fill = tree_color
		end
		return tree
	end
	function M:deleteTree()
		self.currentTree:removeSelf()
		self.currentTree = nil
	end
	
	function M:setTree(level)
		if not self.currentTree then
			self.currentTree = self:createTree(level)
		else
			transition.to(self.currentTree, {time = 500, alpha = 0, function()
				self:deleteTree()
				self.currentTree = self:createTree(level)
				
			end})
		end
	end
end

return M 