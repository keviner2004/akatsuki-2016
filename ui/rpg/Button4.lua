local NineBtn = require("ui.NineSliceButton")
local Button = {}

Button.new = function(w, h, options)
    local button = NineBtn.new("rpg/Button/4", "rpg/Button/4", w, h, options)
    return button
end

return Button