local NineBtn = require("ui.NineSliceButton")
local Panel = {}

Panel.new = function(w, h, options)
    local panel = NineBtn.new("rpg/Panel/1", "rpg/Panel/1", w, h, options)
    return panel
end

return Panel