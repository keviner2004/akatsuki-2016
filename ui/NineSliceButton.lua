local widget = require("widget")
local Sprite = require("Sprite")
local logger = require("logger")
local gameConfig = require("gameConfig")
local ScaleText = require("ui.ScaleText")
local sfx = require("sfx")
local TAG = "9-slice"
--local pathOfThisFile = ...
--print("Path! "..pathOfThisFile)
local NineSliceButton = {}

NineSliceButton.new = function(name1, name2, w, h, options)
    local path1 = string.format("ui/%s", name1)
    local path2 = string.format("ui/%s", name2)
    local button = display.newGroup()

    local _w = math.floor(w)
    local _h = math.floor(h)

    logger:info(TAG, "New 9-slice %s with w:%d, h:%d ", name1, _w, _h)

    button.disableClickSound = (options and options.disableClickSound) or false
    --[[
    local function onEvent(event)
        button.buttonView:onTouch(event)
    end
    --]]

    button.createView = function(self, w, h)
        --print("!!!!!!!!!", Sprite.myImageSheet)
        self.w = w
        self.h = h
        self.buttonView = widget.newButton({
            width = w,
            height = h,
            sheet = Sprite.myImageSheet,
            topLeftFrame = Sprite.getFrameIndex(string.format("%s/1", path1)),
            topMiddleFrame = Sprite.getFrameIndex(string.format("%s/2", path1)),
            topRightFrame = Sprite.getFrameIndex(string.format("%s/3", path1)),
            middleLeftFrame = Sprite.getFrameIndex(string.format("%s/4", path1)),
            middleFrame = Sprite.getFrameIndex(string.format("%s/5", path1)),
            middleRightFrame = Sprite.getFrameIndex(string.format("%s/6", path1)),
            bottomLeftFrame = Sprite.getFrameIndex(string.format("%s/7", path1)),
            bottomMiddleFrame = Sprite.getFrameIndex(string.format("%s/8", path1)),
            bottomRightFrame = Sprite.getFrameIndex(string.format("%s/9", path1)),
            topLeftOverFrame = Sprite.getFrameIndex(string.format("%s/1", path2)),
            topMiddleOverFrame = Sprite.getFrameIndex(string.format("%s/2", path2)),
            topRightOverFrame = Sprite.getFrameIndex(string.format("%s/3", path2)),
            middleLeftOverFrame = Sprite.getFrameIndex(string.format("%s/4", path2)),
            middleOverFrame = Sprite.getFrameIndex(string.format("%s/5", path2)),
            middleRightOverFrame = Sprite.getFrameIndex(string.format("%s/6", path2)),
            bottomLeftOverFrame = Sprite.getFrameIndex(string.format("%s/7", path2)),
            bottomMiddleOverFrame = Sprite.getFrameIndex(string.format("%s/8", path2)),
            bottomRightOverFrame = Sprite.getFrameIndex(string.format("%s/9", path2)),
            --onEvent = onEvent,
            --isEnabled = false,
        })
        self:insert(self.buttonView)
        self.buttonView.x = 0
        self.buttonView.y = 0
        
    end

    button.propagating = true
    button:createView(_w, _h)

    function button:setText(text, font, fontSize)
        if not text then
            text = ""
        end
        if not font then
            font = "kenvector_future_thin"
        end
        if not fontSize then
            fontSize = 17
        end

        if self.buttonText then
          self.buttonText:removeSelf()
          self.buttonText = nil
        end
        self.buttonText = ScaleText.new({
          text = text,
          x = 0,
          y = 0,
          font = font,
          fontSize = fontSize
        })
        self.buttonText.x = 0
        self.buttonText.y = 0
        button:insert(self.buttonText)
    end

    button.originButtonTouch = button.buttonView.touch

    function button.buttonView:touch(event)
    --function button.buttonView:onTouch(event)
        logger:info(TAG, "touch: "..event.phase.."/"..event.phase.." on "..self.id)
        if event.phase == "ended" then
            button:playSound()
            if button.click then
              button.click(event)
            end
        end
        button:onTouch(event)
        button.originButtonTouch(button.buttonView, event)
        --return false treat as background
        return button.propagating --false
    end

    --button:addEventListener("touch", self)

    function button:playSound()
        if not button.disableClickSound then
            --sfx:play(self.pressSound)
        end
    end

    function button:onTouch(event)
      --logger:info(TAG, "button:onTouch")
    end

    function button:setWidth(w)
        --print("set w ", w)
        self.w = w
        if button.buttonView.x then
            button.buttonView:removeSelf()
        end

        if w > 0 then
            self:createView(w, self.h)
        end
        --[[
        if w == 0 then
            self.xScale = 0.001
            self.isVisible = false
        else
            self.isVisible = true
            self.xScale = w/self.width
        end
        --]]
    end

    function button:setHeight(h)
        self.h = h
        if button.buttonView.x then
            button.buttonView:removeSelf()
        end

        if h > 0 then
            self:createView(self.w, h)
        end
        --[[if h == 0 then
            self.yScale = 0.001
            self.isVisible = false
        else
            self.yScale = h/self.height
            self.isVisible = true
        end--]]
    end
    return button
end

return NineSliceButton
