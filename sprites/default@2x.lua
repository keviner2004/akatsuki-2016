--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:64e6a9f7bdfe56784c6182e4e577dbf4:496f704fa6a0b1285c5ea86cdb896877:b880b1d758040f44ea470386dc6fcf1e$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- GHOST
            x=1204,
            y=1238,
            width=440,
            height=580,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 439,
            sourceHeight = 579
        },
        {
            -- LINE
            x=2,
            y=2,
            width=2348,
            height=360,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 2348,
            sourceHeight = 359
        },
        {
            -- Shape
            x=3084,
            y=1202,
            width=256,
            height=256,

        },
        {
            -- Triangle Copy
            x=3756,
            y=1204,
            width=128,
            height=222,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 128,
            sourceHeight = 221
        },
        {
            -- Triangle
            x=3570,
            y=1656,
            width=128,
            height=222,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 128,
            sourceHeight = 221
        },
        {
            -- bottle/bottle
            x=3624,
            y=296,
            width=432,
            height=372,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 431,
            sourceHeight = 371
        },
        {
            -- bottle/gb1
            x=2330,
            y=506,
            width=424,
            height=528,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 424,
            sourceHeight = 527
        },
        {
            -- bottle/gb2
            x=3030,
            y=670,
            width=424,
            height=528,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 424,
            sourceHeight = 527
        },
        {
            -- bottle/gb3
            x=3458,
            y=672,
            width=424,
            height=528,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 424,
            sourceHeight = 527
        },
        {
            -- bottle/gb4
            x=1934,
            y=1096,
            width=424,
            height=528,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 424,
            sourceHeight = 527
        },
        {
            -- bottle/gb5
            x=2362,
            y=1038,
            width=424,
            height=528,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 424,
            sourceHeight = 527
        },
        {
            -- c1
            x=3394,
            y=1708,
            width=172,
            height=172,

        },
        {
            -- c2
            x=3888,
            y=1116,
            width=172,
            height=172,

        },
        {
            -- c3
            x=3888,
            y=1292,
            width=172,
            height=172,

        },
        {
            -- c4
            x=2790,
            y=1030,
            width=236,
            height=236,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 235,
            sourceHeight = 235
        },
        {
            -- c5
            x=2354,
            y=266,
            width=814,
            height=236,

        },
        {
            -- cha/cha1
            x=3172,
            y=266,
            width=448,
            height=400,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 447,
            sourceHeight = 399
        },
        {
            -- charater/1
            x=912,
            y=610,
            width=446,
            height=624,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 445,
            sourceHeight = 624
        },
        {
            -- charater/2
            x=1362,
            y=610,
            width=444,
            height=624,

        },
        {
            -- charater/3
            x=756,
            y=1238,
            width=444,
            height=624,

        },
        {
            -- g1
            x=3706,
            y=1448,
            width=122,
            height=204,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 121,
            sourceHeight = 203
        },
        {
            -- g2
            x=2096,
            y=840,
            width=162,
            height=206,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 161,
            sourceHeight = 205
        },
        {
            -- g3
            x=3548,
            y=1448,
            width=154,
            height=204,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 153,
            sourceHeight = 203
        },
        {
            -- gameover
            x=2354,
            y=2,
            width=1266,
            height=260,

        },
        {
            -- go
            x=3344,
            y=1204,
            width=408,
            height=240,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 407,
            sourceHeight = 240
        },
        {
            -- gototree
            x=2398,
            y=1570,
            width=404,
            height=340,

        },
        {
            -- p/pic1
            x=4028,
            y=1658,
            width=62,
            height=82,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 61,
            sourceHeight = 82
        },
        {
            -- p/pic2
            x=3084,
            y=1462,
            width=60,
            height=82,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 59,
            sourceHeight = 82
        },
        {
            -- p/pic3
            x=2262,
            y=840,
            width=60,
            height=82,

        },
        {
            -- p/pic4
            x=1810,
            y=1096,
            width=78,
            height=114,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 78,
            sourceHeight = 113
        },
        {
            -- p/pic5
            x=3856,
            y=1652,
            width=78,
            height=114,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 78,
            sourceHeight = 113
        },
        {
            -- pop/pop
            x=2790,
            y=1270,
            width=290,
            height=290,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 289,
            sourceHeight = 289
        },
        {
            -- pop/pop2
            x=1934,
            y=1628,
            width=460,
            height=290,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 460,
            sourceHeight = 289
        },
        {
            -- pop/pop3
            x=3624,
            y=2,
            width=466,
            height=290,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 465,
            sourceHeight = 289
        },
        {
            -- quest
            x=2806,
            y=1564,
            width=378,
            height=322,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 377,
            sourceHeight = 322
        },
        {
            -- ready
            x=912,
            y=366,
            width=838,
            height=240,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 838,
            sourceHeight = 239
        },
        {
            -- t/tree0
            x=3938,
            y=1658,
            width=86,
            height=142,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 85,
            sourceHeight = 142
        },
        {
            -- t/tree1
            x=3030,
            y=506,
            width=98,
            height=144,

        },
        {
            -- t/tree10
            x=1648,
            y=1238,
            width=282,
            height=586,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 281,
            sourceHeight = 586
        },
        {
            -- t/tree2
            x=3832,
            y=1468,
            width=104,
            height=180,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 103,
            sourceHeight = 180
        },
        {
            -- t/tree3
            x=3940,
            y=1468,
            width=124,
            height=186,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 123,
            sourceHeight = 185
        },
        {
            -- t/tree4
            x=3702,
            y=1656,
            width=150,
            height=186,

        },
        {
            -- t/tree5
            x=3394,
            y=1448,
            width=150,
            height=256,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 150,
            sourceHeight = 255
        },
        {
            -- t/tree6
            x=3188,
            y=1462,
            width=202,
            height=368,

        },
        {
            -- t/tree7
            x=3886,
            y=672,
            width=202,
            height=402,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 202,
            sourceHeight = 401
        },
        {
            -- t/tree8
            x=2096,
            y=366,
            width=230,
            height=470,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 229,
            sourceHeight = 469
        },
        {
            -- t/tree9
            x=2758,
            y=506,
            width=268,
            height=520,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 268,
            sourceHeight = 519
        },
        {
            -- treepop
            x=1810,
            y=366,
            width=282,
            height=726,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 281,
            sourceHeight = 725
        },
        {
            -- treepop2
            x=2,
            y=366,
            width=906,
            height=850,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 905,
            sourceHeight = 849
        },
        {
            -- ui/rpg/Button/4/1
            x=3702,
            y=1846,
            width=86,
            height=34,

        },
        {
            -- ui/rpg/Button/4/2
            x=3030,
            y=1202,
            width=2,
            height=34,

        },
        {
            -- ui/rpg/Button/4/3
            x=3886,
            y=1078,
            width=190,
            height=34,

        },
        {
            -- ui/rpg/Button/4/4
            x=2398,
            y=1914,
            width=86,
            height=2,

        },
        {
            -- ui/rpg/Button/4/5
            x=2790,
            y=1564,
            width=2,
            height=2,

        },
        {
            -- ui/rpg/Button/4/6
            x=756,
            y=1866,
            width=190,
            height=2,

        },
        {
            -- ui/rpg/Button/4/7
            x=2096,
            y=1050,
            width=86,
            height=40,

        },
        {
            -- ui/rpg/Button/4/8
            x=2362,
            y=1570,
            width=2,
            height=40,

        },
        {
            -- ui/rpg/Button/4/9
            x=1204,
            y=1822,
            width=190,
            height=40,

        },
        {
            -- ui/rpg/Button/8/1
            x=2186,
            y=1050,
            width=86,
            height=34,

        },
        {
            -- ui/rpg/Button/8/2
            x=3030,
            y=1202,
            width=2,
            height=34,

        },
        {
            -- ui/rpg/Button/8/3
            x=3188,
            y=1834,
            width=190,
            height=34,

        },
        {
            -- ui/rpg/Button/8/4
            x=2398,
            y=1914,
            width=86,
            height=2,

        },
        {
            -- ui/rpg/Button/8/5
            x=2790,
            y=1564,
            width=2,
            height=2,

        },
        {
            -- ui/rpg/Button/8/6
            x=756,
            y=1866,
            width=190,
            height=2,

        },
        {
            -- ui/rpg/Button/8/7
            x=196,
            y=1886,
            width=86,
            height=32,

        },
        {
            -- ui/rpg/Button/8/8
            x=286,
            y=1886,
            width=2,
            height=32,

        },
        {
            -- ui/rpg/Button/8/9
            x=2,
            y=1886,
            width=190,
            height=32,

        },
        {
            -- ui/rpg/Panel/1/1
            x=1398,
            y=1822,
            width=110,
            height=50,

        },
        {
            -- ui/rpg/Panel/1/2
            x=4060,
            y=296,
            width=2,
            height=50,

        },
        {
            -- ui/rpg/Panel/1/3
            x=1754,
            y=366,
            width=18,
            height=50,

        },
        {
            -- ui/rpg/Panel/1/4
            x=756,
            y=1220,
            width=110,
            height=2,

        },
        {
            -- ui/rpg/Panel/1/5
            x=2796,
            y=1564,
            width=2,
            height=2,

        },
        {
            -- ui/rpg/Panel/1/6
            x=3548,
            y=1656,
            width=18,
            height=2,

        },
        {
            -- ui/rpg/Panel/1/7
            x=1512,
            y=1822,
            width=110,
            height=66,

        },
        {
            -- ui/rpg/Panel/1/8
            x=2330,
            y=366,
            width=2,
            height=66,

        },
        {
            -- ui/rpg/Panel/1/9
            x=1626,
            y=1822,
            width=18,
            height=66,

        },
        {
            -- win
            x=2,
            y=1220,
            width=750,
            height=662,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 749,
            sourceHeight = 662
        },
    },
    
    sheetContentWidth = 4092,
    sheetContentHeight = 1920
}

SheetInfo.frameIndex =
{

    ["GHOST"] = 1,
    ["LINE"] = 2,
    ["Shape"] = 3,
    ["Triangle Copy"] = 4,
    ["Triangle"] = 5,
    ["bottle/bottle"] = 6,
    ["bottle/gb1"] = 7,
    ["bottle/gb2"] = 8,
    ["bottle/gb3"] = 9,
    ["bottle/gb4"] = 10,
    ["bottle/gb5"] = 11,
    ["c1"] = 12,
    ["c2"] = 13,
    ["c3"] = 14,
    ["c4"] = 15,
    ["c5"] = 16,
    ["cha/cha1"] = 17,
    ["charater/1"] = 18,
    ["charater/2"] = 19,
    ["charater/3"] = 20,
    ["g1"] = 21,
    ["g2"] = 22,
    ["g3"] = 23,
    ["gameover"] = 24,
    ["go"] = 25,
    ["gototree"] = 26,
    ["p/pic1"] = 27,
    ["p/pic2"] = 28,
    ["p/pic3"] = 29,
    ["p/pic4"] = 30,
    ["p/pic5"] = 31,
    ["pop/pop"] = 32,
    ["pop/pop2"] = 33,
    ["pop/pop3"] = 34,
    ["quest"] = 35,
    ["ready"] = 36,
    ["t/tree0"] = 37,
    ["t/tree1"] = 38,
    ["t/tree10"] = 39,
    ["t/tree2"] = 40,
    ["t/tree3"] = 41,
    ["t/tree4"] = 42,
    ["t/tree5"] = 43,
    ["t/tree6"] = 44,
    ["t/tree7"] = 45,
    ["t/tree8"] = 46,
    ["t/tree9"] = 47,
    ["treepop"] = 48,
    ["treepop2"] = 49,
    ["ui/rpg/Button/4/1"] = 50,
    ["ui/rpg/Button/4/2"] = 51,
    ["ui/rpg/Button/4/3"] = 52,
    ["ui/rpg/Button/4/4"] = 53,
    ["ui/rpg/Button/4/5"] = 54,
    ["ui/rpg/Button/4/6"] = 55,
    ["ui/rpg/Button/4/7"] = 56,
    ["ui/rpg/Button/4/8"] = 57,
    ["ui/rpg/Button/4/9"] = 58,
    ["ui/rpg/Button/8/1"] = 59,
    ["ui/rpg/Button/8/2"] = 60,
    ["ui/rpg/Button/8/3"] = 61,
    ["ui/rpg/Button/8/4"] = 62,
    ["ui/rpg/Button/8/5"] = 63,
    ["ui/rpg/Button/8/6"] = 64,
    ["ui/rpg/Button/8/7"] = 65,
    ["ui/rpg/Button/8/8"] = 66,
    ["ui/rpg/Button/8/9"] = 67,
    ["ui/rpg/Panel/1/1"] = 68,
    ["ui/rpg/Panel/1/2"] = 69,
    ["ui/rpg/Panel/1/3"] = 70,
    ["ui/rpg/Panel/1/4"] = 71,
    ["ui/rpg/Panel/1/5"] = 72,
    ["ui/rpg/Panel/1/6"] = 73,
    ["ui/rpg/Panel/1/7"] = 74,
    ["ui/rpg/Panel/1/8"] = 75,
    ["ui/rpg/Panel/1/9"] = 76,
    ["win"] = 77,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
