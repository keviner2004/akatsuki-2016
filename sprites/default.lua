--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:64e6a9f7bdfe56784c6182e4e577dbf4:496f704fa6a0b1285c5ea86cdb896877:b880b1d758040f44ea470386dc6fcf1e$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- GHOST
            x=602,
            y=619,
            width=220,
            height=290,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 220,
            sourceHeight = 289
        },
        {
            -- LINE
            x=1,
            y=1,
            width=1174,
            height=180,

        },
        {
            -- Shape
            x=1542,
            y=601,
            width=128,
            height=128,

        },
        {
            -- Triangle Copy
            x=1878,
            y=602,
            width=64,
            height=111,

        },
        {
            -- Triangle
            x=1785,
            y=828,
            width=64,
            height=111,

        },
        {
            -- bottle/bottle
            x=1812,
            y=148,
            width=216,
            height=186,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 215,
            sourceHeight = 185
        },
        {
            -- bottle/gb1
            x=1165,
            y=253,
            width=212,
            height=264,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 212,
            sourceHeight = 263
        },
        {
            -- bottle/gb2
            x=1515,
            y=335,
            width=212,
            height=264,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 212,
            sourceHeight = 263
        },
        {
            -- bottle/gb3
            x=1729,
            y=336,
            width=212,
            height=264,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 212,
            sourceHeight = 263
        },
        {
            -- bottle/gb4
            x=967,
            y=548,
            width=212,
            height=264,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 212,
            sourceHeight = 263
        },
        {
            -- bottle/gb5
            x=1181,
            y=519,
            width=212,
            height=264,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 212,
            sourceHeight = 263
        },
        {
            -- c1
            x=1697,
            y=854,
            width=86,
            height=86,

        },
        {
            -- c2
            x=1944,
            y=558,
            width=86,
            height=86,

        },
        {
            -- c3
            x=1944,
            y=646,
            width=86,
            height=86,

        },
        {
            -- c4
            x=1395,
            y=515,
            width=118,
            height=118,

        },
        {
            -- c5
            x=1177,
            y=133,
            width=407,
            height=118,

        },
        {
            -- cha/cha1
            x=1586,
            y=133,
            width=224,
            height=200,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 224,
            sourceHeight = 199
        },
        {
            -- charater/1
            x=456,
            y=305,
            width=223,
            height=312,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 222,
            sourceHeight = 312
        },
        {
            -- charater/2
            x=681,
            y=305,
            width=222,
            height=312,

        },
        {
            -- charater/3
            x=378,
            y=619,
            width=222,
            height=312,

        },
        {
            -- g1
            x=1853,
            y=724,
            width=61,
            height=102,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 60,
            sourceHeight = 101
        },
        {
            -- g2
            x=1048,
            y=420,
            width=81,
            height=103,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 103
        },
        {
            -- g3
            x=1774,
            y=724,
            width=77,
            height=102,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 76,
            sourceHeight = 102
        },
        {
            -- gameover
            x=1177,
            y=1,
            width=633,
            height=130,

        },
        {
            -- go
            x=1672,
            y=602,
            width=204,
            height=120,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 203,
            sourceHeight = 120
        },
        {
            -- gototree
            x=1199,
            y=785,
            width=202,
            height=170,

        },
        {
            -- p/pic1
            x=2014,
            y=829,
            width=31,
            height=41,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 30,
            sourceHeight = 41
        },
        {
            -- p/pic2
            x=1542,
            y=731,
            width=30,
            height=41,

        },
        {
            -- p/pic3
            x=1131,
            y=420,
            width=30,
            height=41,

        },
        {
            -- p/pic4
            x=905,
            y=548,
            width=39,
            height=57,

        },
        {
            -- p/pic5
            x=1928,
            y=826,
            width=39,
            height=57,

        },
        {
            -- pop/pop
            x=1395,
            y=635,
            width=145,
            height=145,

        },
        {
            -- pop/pop2
            x=967,
            y=814,
            width=230,
            height=145,

        },
        {
            -- pop/pop3
            x=1812,
            y=1,
            width=233,
            height=145,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 232,
            sourceHeight = 145
        },
        {
            -- quest
            x=1403,
            y=782,
            width=189,
            height=161,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 188,
            sourceHeight = 161
        },
        {
            -- ready
            x=456,
            y=183,
            width=419,
            height=120,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 419,
            sourceHeight = 119
        },
        {
            -- t/tree0
            x=1969,
            y=829,
            width=43,
            height=71,

        },
        {
            -- t/tree1
            x=1515,
            y=253,
            width=49,
            height=72,

        },
        {
            -- t/tree10
            x=824,
            y=619,
            width=141,
            height=293,

        },
        {
            -- t/tree2
            x=1916,
            y=734,
            width=52,
            height=90,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 51,
            sourceHeight = 90
        },
        {
            -- t/tree3
            x=1970,
            y=734,
            width=62,
            height=93,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 61,
            sourceHeight = 93
        },
        {
            -- t/tree4
            x=1851,
            y=828,
            width=75,
            height=93,

        },
        {
            -- t/tree5
            x=1697,
            y=724,
            width=75,
            height=128,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 75,
            sourceHeight = 127
        },
        {
            -- t/tree6
            x=1594,
            y=731,
            width=101,
            height=184,

        },
        {
            -- t/tree7
            x=1943,
            y=336,
            width=101,
            height=201,

        },
        {
            -- t/tree8
            x=1048,
            y=183,
            width=115,
            height=235,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 114,
            sourceHeight = 235
        },
        {
            -- t/tree9
            x=1379,
            y=253,
            width=134,
            height=260,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 134,
            sourceHeight = 259
        },
        {
            -- treepop
            x=905,
            y=183,
            width=141,
            height=363,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 141,
            sourceHeight = 362
        },
        {
            -- treepop2
            x=1,
            y=183,
            width=453,
            height=425,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 452,
            sourceHeight = 425
        },
        {
            -- ui/rpg/Button/4/1
            x=1851,
            y=923,
            width=43,
            height=17,

        },
        {
            -- ui/rpg/Button/4/2
            x=1515,
            y=601,
            width=1,
            height=17,

        },
        {
            -- ui/rpg/Button/4/3
            x=1943,
            y=539,
            width=95,
            height=17,

        },
        {
            -- ui/rpg/Button/4/4
            x=1199,
            y=957,
            width=43,
            height=1,

        },
        {
            -- ui/rpg/Button/4/5
            x=1395,
            y=782,
            width=1,
            height=1,

        },
        {
            -- ui/rpg/Button/4/6
            x=378,
            y=933,
            width=95,
            height=1,

        },
        {
            -- ui/rpg/Button/4/7
            x=1048,
            y=525,
            width=43,
            height=20,

        },
        {
            -- ui/rpg/Button/4/8
            x=1181,
            y=785,
            width=1,
            height=20,

        },
        {
            -- ui/rpg/Button/4/9
            x=602,
            y=911,
            width=95,
            height=20,

        },
        {
            -- ui/rpg/Button/8/1
            x=1093,
            y=525,
            width=43,
            height=17,

        },
        {
            -- ui/rpg/Button/8/2
            x=1515,
            y=601,
            width=1,
            height=17,

        },
        {
            -- ui/rpg/Button/8/3
            x=1594,
            y=917,
            width=95,
            height=17,

        },
        {
            -- ui/rpg/Button/8/4
            x=1199,
            y=957,
            width=43,
            height=1,

        },
        {
            -- ui/rpg/Button/8/5
            x=1395,
            y=782,
            width=1,
            height=1,

        },
        {
            -- ui/rpg/Button/8/6
            x=378,
            y=933,
            width=95,
            height=1,

        },
        {
            -- ui/rpg/Button/8/7
            x=98,
            y=943,
            width=43,
            height=16,

        },
        {
            -- ui/rpg/Button/8/8
            x=143,
            y=943,
            width=1,
            height=16,

        },
        {
            -- ui/rpg/Button/8/9
            x=1,
            y=943,
            width=95,
            height=16,

        },
        {
            -- ui/rpg/Panel/1/1
            x=699,
            y=911,
            width=55,
            height=25,

        },
        {
            -- ui/rpg/Panel/1/2
            x=2030,
            y=148,
            width=1,
            height=25,

        },
        {
            -- ui/rpg/Panel/1/3
            x=877,
            y=183,
            width=9,
            height=25,

        },
        {
            -- ui/rpg/Panel/1/4
            x=378,
            y=610,
            width=55,
            height=1,

        },
        {
            -- ui/rpg/Panel/1/5
            x=1398,
            y=782,
            width=1,
            height=1,

        },
        {
            -- ui/rpg/Panel/1/6
            x=1774,
            y=828,
            width=9,
            height=1,

        },
        {
            -- ui/rpg/Panel/1/7
            x=756,
            y=911,
            width=55,
            height=33,

        },
        {
            -- ui/rpg/Panel/1/8
            x=1165,
            y=183,
            width=1,
            height=33,

        },
        {
            -- ui/rpg/Panel/1/9
            x=813,
            y=911,
            width=9,
            height=33,

        },
        {
            -- win
            x=1,
            y=610,
            width=375,
            height=331,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 374,
            sourceHeight = 331
        },
    },
    
    sheetContentWidth = 2046,
    sheetContentHeight = 960
}

SheetInfo.frameIndex =
{

    ["GHOST"] = 1,
    ["LINE"] = 2,
    ["Shape"] = 3,
    ["Triangle Copy"] = 4,
    ["Triangle"] = 5,
    ["bottle/bottle"] = 6,
    ["bottle/gb1"] = 7,
    ["bottle/gb2"] = 8,
    ["bottle/gb3"] = 9,
    ["bottle/gb4"] = 10,
    ["bottle/gb5"] = 11,
    ["c1"] = 12,
    ["c2"] = 13,
    ["c3"] = 14,
    ["c4"] = 15,
    ["c5"] = 16,
    ["cha/cha1"] = 17,
    ["charater/1"] = 18,
    ["charater/2"] = 19,
    ["charater/3"] = 20,
    ["g1"] = 21,
    ["g2"] = 22,
    ["g3"] = 23,
    ["gameover"] = 24,
    ["go"] = 25,
    ["gototree"] = 26,
    ["p/pic1"] = 27,
    ["p/pic2"] = 28,
    ["p/pic3"] = 29,
    ["p/pic4"] = 30,
    ["p/pic5"] = 31,
    ["pop/pop"] = 32,
    ["pop/pop2"] = 33,
    ["pop/pop3"] = 34,
    ["quest"] = 35,
    ["ready"] = 36,
    ["t/tree0"] = 37,
    ["t/tree1"] = 38,
    ["t/tree10"] = 39,
    ["t/tree2"] = 40,
    ["t/tree3"] = 41,
    ["t/tree4"] = 42,
    ["t/tree5"] = 43,
    ["t/tree6"] = 44,
    ["t/tree7"] = 45,
    ["t/tree8"] = 46,
    ["t/tree9"] = 47,
    ["treepop"] = 48,
    ["treepop2"] = 49,
    ["ui/rpg/Button/4/1"] = 50,
    ["ui/rpg/Button/4/2"] = 51,
    ["ui/rpg/Button/4/3"] = 52,
    ["ui/rpg/Button/4/4"] = 53,
    ["ui/rpg/Button/4/5"] = 54,
    ["ui/rpg/Button/4/6"] = 55,
    ["ui/rpg/Button/4/7"] = 56,
    ["ui/rpg/Button/4/8"] = 57,
    ["ui/rpg/Button/4/9"] = 58,
    ["ui/rpg/Button/8/1"] = 59,
    ["ui/rpg/Button/8/2"] = 60,
    ["ui/rpg/Button/8/3"] = 61,
    ["ui/rpg/Button/8/4"] = 62,
    ["ui/rpg/Button/8/5"] = 63,
    ["ui/rpg/Button/8/6"] = 64,
    ["ui/rpg/Button/8/7"] = 65,
    ["ui/rpg/Button/8/8"] = 66,
    ["ui/rpg/Button/8/9"] = 67,
    ["ui/rpg/Panel/1/1"] = 68,
    ["ui/rpg/Panel/1/2"] = 69,
    ["ui/rpg/Panel/1/3"] = 70,
    ["ui/rpg/Panel/1/4"] = 71,
    ["ui/rpg/Panel/1/5"] = 72,
    ["ui/rpg/Panel/1/6"] = 73,
    ["ui/rpg/Panel/1/7"] = 74,
    ["ui/rpg/Panel/1/8"] = 75,
    ["ui/rpg/Panel/1/9"] = 76,
    ["win"] = 77,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
