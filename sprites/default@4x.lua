--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:64e6a9f7bdfe56784c6182e4e577dbf4:496f704fa6a0b1285c5ea86cdb896877:b880b1d758040f44ea470386dc6fcf1e$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- GHOST
            x=2408,
            y=2476,
            width=880,
            height=1160,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 878,
            sourceHeight = 1157
        },
        {
            -- LINE
            x=4,
            y=4,
            width=4696,
            height=720,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 4695,
            sourceHeight = 718
        },
        {
            -- Shape
            x=6168,
            y=2404,
            width=512,
            height=512,

        },
        {
            -- Triangle Copy
            x=7512,
            y=2408,
            width=256,
            height=444,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 256,
            sourceHeight = 442
        },
        {
            -- Triangle
            x=7140,
            y=3312,
            width=256,
            height=444,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 256,
            sourceHeight = 442
        },
        {
            -- bottle/bottle
            x=7248,
            y=592,
            width=864,
            height=744,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 861,
            sourceHeight = 741
        },
        {
            -- bottle/gb1
            x=4660,
            y=1012,
            width=848,
            height=1056,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 847,
            sourceHeight = 1053
        },
        {
            -- bottle/gb2
            x=6060,
            y=1340,
            width=848,
            height=1056,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 847,
            sourceHeight = 1053
        },
        {
            -- bottle/gb3
            x=6916,
            y=1344,
            width=848,
            height=1056,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 847,
            sourceHeight = 1053
        },
        {
            -- bottle/gb4
            x=3868,
            y=2192,
            width=848,
            height=1056,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 847,
            sourceHeight = 1053
        },
        {
            -- bottle/gb5
            x=4724,
            y=2076,
            width=848,
            height=1056,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 847,
            sourceHeight = 1053
        },
        {
            -- c1
            x=6788,
            y=3416,
            width=344,
            height=344,

        },
        {
            -- c2
            x=7776,
            y=2232,
            width=344,
            height=344,

        },
        {
            -- c3
            x=7776,
            y=2584,
            width=344,
            height=344,

        },
        {
            -- c4
            x=5580,
            y=2060,
            width=472,
            height=472,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 470,
            sourceHeight = 470
        },
        {
            -- c5
            x=4708,
            y=532,
            width=1628,
            height=472,

        },
        {
            -- cha/cha1
            x=6344,
            y=532,
            width=896,
            height=800,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 894,
            sourceHeight = 797
        },
        {
            -- charater/1
            x=1824,
            y=1220,
            width=892,
            height=1248,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 889,
            sourceHeight = 1248
        },
        {
            -- charater/2
            x=2724,
            y=1220,
            width=888,
            height=1248,

        },
        {
            -- charater/3
            x=1512,
            y=2476,
            width=888,
            height=1248,

        },
        {
            -- g1
            x=7412,
            y=2896,
            width=244,
            height=408,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 241,
            sourceHeight = 405
        },
        {
            -- g2
            x=4192,
            y=1680,
            width=324,
            height=412,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 321,
            sourceHeight = 410
        },
        {
            -- g3
            x=7096,
            y=2896,
            width=308,
            height=408,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 305,
            sourceHeight = 406
        },
        {
            -- gameover
            x=4708,
            y=4,
            width=2532,
            height=520,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 2531,
            sourceHeight = 519
        },
        {
            -- go
            x=6688,
            y=2408,
            width=816,
            height=480,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 813,
            sourceHeight = 480
        },
        {
            -- gototree
            x=4796,
            y=3140,
            width=808,
            height=680,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 807,
            sourceHeight = 680
        },
        {
            -- p/pic1
            x=8056,
            y=3316,
            width=124,
            height=164,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 121,
            sourceHeight = 163
        },
        {
            -- p/pic2
            x=6168,
            y=2924,
            width=120,
            height=164,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 118,
            sourceHeight = 164
        },
        {
            -- p/pic3
            x=4524,
            y=1680,
            width=120,
            height=164,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 120,
            sourceHeight = 163
        },
        {
            -- p/pic4
            x=3620,
            y=2192,
            width=156,
            height=228,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 156,
            sourceHeight = 226
        },
        {
            -- p/pic5
            x=7712,
            y=3304,
            width=156,
            height=228,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 156,
            sourceHeight = 226
        },
        {
            -- pop/pop
            x=5580,
            y=2540,
            width=580,
            height=580,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 578,
            sourceHeight = 578
        },
        {
            -- pop/pop2
            x=3868,
            y=3256,
            width=920,
            height=580,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 920,
            sourceHeight = 578
        },
        {
            -- pop/pop3
            x=7248,
            y=4,
            width=932,
            height=580,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 929,
            sourceHeight = 578
        },
        {
            -- quest
            x=5612,
            y=3128,
            width=756,
            height=644,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 753,
            sourceHeight = 644
        },
        {
            -- ready
            x=1824,
            y=732,
            width=1676,
            height=480,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 1675,
            sourceHeight = 477
        },
        {
            -- t/tree0
            x=7876,
            y=3316,
            width=172,
            height=284,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 170,
            sourceHeight = 284
        },
        {
            -- t/tree1
            x=6060,
            y=1012,
            width=196,
            height=288,

        },
        {
            -- t/tree10
            x=3296,
            y=2476,
            width=564,
            height=1172,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 562,
            sourceHeight = 1172
        },
        {
            -- t/tree2
            x=7664,
            y=2936,
            width=208,
            height=360,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 205,
            sourceHeight = 359
        },
        {
            -- t/tree3
            x=7880,
            y=2936,
            width=248,
            height=372,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 245,
            sourceHeight = 370
        },
        {
            -- t/tree4
            x=7404,
            y=3312,
            width=300,
            height=372,

        },
        {
            -- t/tree5
            x=6788,
            y=2896,
            width=300,
            height=512,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 300,
            sourceHeight = 509
        },
        {
            -- t/tree6
            x=6376,
            y=2924,
            width=404,
            height=736,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 404,
            sourceHeight = 735
        },
        {
            -- t/tree7
            x=7772,
            y=1344,
            width=404,
            height=804,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 404,
            sourceHeight = 802
        },
        {
            -- t/tree8
            x=4192,
            y=732,
            width=460,
            height=940,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 457,
            sourceHeight = 938
        },
        {
            -- t/tree9
            x=5516,
            y=1012,
            width=536,
            height=1040,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 536,
            sourceHeight = 1037
        },
        {
            -- treepop
            x=3620,
            y=732,
            width=564,
            height=1452,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 562,
            sourceHeight = 1449
        },
        {
            -- treepop2
            x=4,
            y=732,
            width=1812,
            height=1700,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 1809,
            sourceHeight = 1698
        },
        {
            -- ui/rpg/Button/4/1
            x=7404,
            y=3692,
            width=172,
            height=68,

        },
        {
            -- ui/rpg/Button/4/2
            x=6060,
            y=2404,
            width=4,
            height=68,

        },
        {
            -- ui/rpg/Button/4/3
            x=7772,
            y=2156,
            width=380,
            height=68,

        },
        {
            -- ui/rpg/Button/4/4
            x=4796,
            y=3828,
            width=172,
            height=4,

        },
        {
            -- ui/rpg/Button/4/5
            x=5580,
            y=3128,
            width=4,
            height=4,

        },
        {
            -- ui/rpg/Button/4/6
            x=1512,
            y=3732,
            width=380,
            height=4,

        },
        {
            -- ui/rpg/Button/4/7
            x=4192,
            y=2100,
            width=172,
            height=80,

        },
        {
            -- ui/rpg/Button/4/8
            x=4724,
            y=3140,
            width=4,
            height=80,

        },
        {
            -- ui/rpg/Button/4/9
            x=2408,
            y=3644,
            width=380,
            height=80,

        },
        {
            -- ui/rpg/Button/8/1
            x=4372,
            y=2100,
            width=172,
            height=68,

        },
        {
            -- ui/rpg/Button/8/2
            x=6060,
            y=2404,
            width=4,
            height=68,

        },
        {
            -- ui/rpg/Button/8/3
            x=6376,
            y=3668,
            width=380,
            height=68,

        },
        {
            -- ui/rpg/Button/8/4
            x=4796,
            y=3828,
            width=172,
            height=4,

        },
        {
            -- ui/rpg/Button/8/5
            x=5580,
            y=3128,
            width=4,
            height=4,

        },
        {
            -- ui/rpg/Button/8/6
            x=1512,
            y=3732,
            width=380,
            height=4,

        },
        {
            -- ui/rpg/Button/8/7
            x=392,
            y=3772,
            width=172,
            height=64,

        },
        {
            -- ui/rpg/Button/8/8
            x=572,
            y=3772,
            width=4,
            height=64,

        },
        {
            -- ui/rpg/Button/8/9
            x=4,
            y=3772,
            width=380,
            height=64,

        },
        {
            -- ui/rpg/Panel/1/1
            x=2796,
            y=3644,
            width=220,
            height=100,

        },
        {
            -- ui/rpg/Panel/1/2
            x=8120,
            y=592,
            width=4,
            height=100,

        },
        {
            -- ui/rpg/Panel/1/3
            x=3508,
            y=732,
            width=36,
            height=100,

        },
        {
            -- ui/rpg/Panel/1/4
            x=1512,
            y=2440,
            width=220,
            height=4,

        },
        {
            -- ui/rpg/Panel/1/5
            x=5592,
            y=3128,
            width=4,
            height=4,

        },
        {
            -- ui/rpg/Panel/1/6
            x=7096,
            y=3312,
            width=36,
            height=4,

        },
        {
            -- ui/rpg/Panel/1/7
            x=3024,
            y=3644,
            width=220,
            height=132,

        },
        {
            -- ui/rpg/Panel/1/8
            x=4660,
            y=732,
            width=4,
            height=132,

        },
        {
            -- ui/rpg/Panel/1/9
            x=3252,
            y=3644,
            width=36,
            height=132,

        },
        {
            -- win
            x=4,
            y=2440,
            width=1500,
            height=1324,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 1497,
            sourceHeight = 1324
        },
    },
    
    sheetContentWidth = 8184,
    sheetContentHeight = 3840
}

SheetInfo.frameIndex =
{

    ["GHOST"] = 1,
    ["LINE"] = 2,
    ["Shape"] = 3,
    ["Triangle Copy"] = 4,
    ["Triangle"] = 5,
    ["bottle/bottle"] = 6,
    ["bottle/gb1"] = 7,
    ["bottle/gb2"] = 8,
    ["bottle/gb3"] = 9,
    ["bottle/gb4"] = 10,
    ["bottle/gb5"] = 11,
    ["c1"] = 12,
    ["c2"] = 13,
    ["c3"] = 14,
    ["c4"] = 15,
    ["c5"] = 16,
    ["cha/cha1"] = 17,
    ["charater/1"] = 18,
    ["charater/2"] = 19,
    ["charater/3"] = 20,
    ["g1"] = 21,
    ["g2"] = 22,
    ["g3"] = 23,
    ["gameover"] = 24,
    ["go"] = 25,
    ["gototree"] = 26,
    ["p/pic1"] = 27,
    ["p/pic2"] = 28,
    ["p/pic3"] = 29,
    ["p/pic4"] = 30,
    ["p/pic5"] = 31,
    ["pop/pop"] = 32,
    ["pop/pop2"] = 33,
    ["pop/pop3"] = 34,
    ["quest"] = 35,
    ["ready"] = 36,
    ["t/tree0"] = 37,
    ["t/tree1"] = 38,
    ["t/tree10"] = 39,
    ["t/tree2"] = 40,
    ["t/tree3"] = 41,
    ["t/tree4"] = 42,
    ["t/tree5"] = 43,
    ["t/tree6"] = 44,
    ["t/tree7"] = 45,
    ["t/tree8"] = 46,
    ["t/tree9"] = 47,
    ["treepop"] = 48,
    ["treepop2"] = 49,
    ["ui/rpg/Button/4/1"] = 50,
    ["ui/rpg/Button/4/2"] = 51,
    ["ui/rpg/Button/4/3"] = 52,
    ["ui/rpg/Button/4/4"] = 53,
    ["ui/rpg/Button/4/5"] = 54,
    ["ui/rpg/Button/4/6"] = 55,
    ["ui/rpg/Button/4/7"] = 56,
    ["ui/rpg/Button/4/8"] = 57,
    ["ui/rpg/Button/4/9"] = 58,
    ["ui/rpg/Button/8/1"] = 59,
    ["ui/rpg/Button/8/2"] = 60,
    ["ui/rpg/Button/8/3"] = 61,
    ["ui/rpg/Button/8/4"] = 62,
    ["ui/rpg/Button/8/5"] = 63,
    ["ui/rpg/Button/8/6"] = 64,
    ["ui/rpg/Button/8/7"] = 65,
    ["ui/rpg/Button/8/8"] = 66,
    ["ui/rpg/Button/8/9"] = 67,
    ["ui/rpg/Panel/1/1"] = 68,
    ["ui/rpg/Panel/1/2"] = 69,
    ["ui/rpg/Panel/1/3"] = 70,
    ["ui/rpg/Panel/1/4"] = 71,
    ["ui/rpg/Panel/1/5"] = 72,
    ["ui/rpg/Panel/1/6"] = 73,
    ["ui/rpg/Panel/1/7"] = 74,
    ["ui/rpg/Panel/1/8"] = 75,
    ["ui/rpg/Panel/1/9"] = 76,
    ["win"] = 77,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
