local lfs = require "lfs"
local logger = require("logger")
local config = require("gameConfig")
local TAG = "audio"
local sfx = {}

sfx.CHANNEL_BG = 1
sfx.CHANNEL_UI = 2

sfx.bg = {
  handle = audio.loadSound( "sounds/bg.mp3" ),
  channel = sfx.CHANNEL_BG
}

sfx.click = {
  handle = audio.loadSound("sounds/click.mp3"),
}

sfx.title = {
	handle = audio.loadSound( "sounds/title.mp3" ),
}

sfx.victory = {
	handle = audio.loadSound( "sounds/victory.mp3" ),
	channel = 32
}

sfx.lev_1_bgm = {
  handle = audio.loadSound("sounds/level_1_bgm.mp3")
}

sfx.laugh_1 = {
  handle = audio.loadSound("sounds/ha1.mp3")
}

sfx.laugh_2 = {
  handle = audio.loadSound("sounds/ha2.mp3")
}

sfx.laugh_3 = {
  handle = audio.loadSound("sounds/ha3.mp3")
}

sfx.boss = {
  handle=audio.loadSound("sounds/boss.mp3")
}

sfx.countdown1 = {
  handle=audio.loadSound("sounds/cutdone1.mp3")
}

sfx.countdown2 = {
  handle=audio.loadSound("sounds/cutdone2.mp3")
}

sfx.countdown3 = {
  handle=audio.loadSound("sounds/cutdone3.mp3")
}

sfx.start = {
  handle=audio.loadSound("sounds/start.mp3")
}

function sfx:initVolumn()
  --local masterVolume = audio.getVolume()
  --print( "volume "..masterVolume )
  audio.setVolume( 0.7, { channel = self.CHANNEL_BG } )  --music track
  audio.setVolume( 0.8, { channel = self.CHANNEL_UI } )  --ui
end

function sfx:init()
  audio.reserveChannels(3)
  self:initVolumn()
end

function sfx:play(name, options)
    if not config.soundOn then
      return
    end
    self:initVolumn()

    if not options then
      options = {}
    end

    local reservedChannel = self[name].channel

    if reservedChannel and reservedChannel ~= 0 then
      audio.stop(reservedChannel)
      options.channel = reservedChannel
    else
      local availableChannel = audio.findFreeChannel( 4 )
      audio.setVolume( 1, { channel=availableChannel } )
      options.channel = availableChannel
    end

    local currentVolumn = audio.getVolume( { channel = options.channel } )

    --logger:debug(TAG, "Play sound %s at channel %d with volume %d", name, options.channel, currentVolumn)


    return audio.play(self[name].handle, options)

end

function sfx:fadeOut(channel, time)
  audio.fadeOut(channel, time)
end

function sfx:stop(channel)
    if not channel then
      audio.stop()
    else
      audio.stop(channel)
    end
end

function sfx:pause(channel)
  audio.pause(channel)
end

function sfx:resume(channel)
  audio.resume(channel)
end

sfx:init()

return sfx
