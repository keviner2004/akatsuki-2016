local composer = require( "composer" )
local Sprite = require("Sprite")
local scene = composer.newScene()
local gameConfig = require("gameConfig")
local sfx = require ("sfx")


-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
local function return1Handler( event )
	if (event.phase == "began") then  
		print ("began")
		local options = {
    effect = "fromTop",
    time = 500
}
sfx:stop(scene.channel)
composer.gotoScene( "scenes.level_select", options )
	end
end
-- create()
local function return2Handler( event )
	if (event.phase == "began") then  
		print ("began")
		local options = {
    effect = "fromTop",
    time = 500
}
sfx:stop(scene.channel)
gameConfig.TitleChannel = -1
composer.gotoScene( "scenes.map", options )
	end
end

function scene:create( event )

    local sceneGroup = self.view
	local group_images= display.newGroup()
	
    -- Code here runs when the scene is first created but has not yet appeared on screen
	local blackboard = display.newImage("imgs/blackboard.png")
    blackboard.xScale = display.contentHeight*1.4/blackboard.height
    blackboard.yScale = display.contentHeight*1.1/blackboard.height
    blackboard.x=display.contentWidth/2
    blackboard.y=display.contentHeight/1.95
	group_images:insert(blackboard)
	sceneGroup:insert(blackboard)
	
	local win = Sprite.new("win")
	win.x=display.contentWidth/2
    win.y=display.contentHeight/2.2
	win.xScale = display.contentWidth/win.width*0.4
    win.yScale = display.contentHeight/win.height*0.6
	win.alpha = 0
	transition.to(win,{time=1000,alpha=1})
    group_images:insert(win)
	sceneGroup:insert(win)
	
	local return1 = Sprite.new("quest")
	return1.x=display.contentWidth/5
    return1.y=display.contentHeight/1.2
	return1.xScale = display.contentWidth/return1.width*0.11
    return1.yScale = display.contentHeight/return1.height*0.2
	return1.alpha = 0
	transition.to(return1,{time=1000,alpha=1,delay=1500})
    group_images:insert(return1)
	sceneGroup:insert(return1)
	return1:addEventListener("touch", return1Handler)
	
	local return2 = Sprite.new("gototree")
	return2.x=display.contentWidth/1.25
    return2.y=display.contentHeight/1.2
	return2.xScale = display.contentWidth/return2.width*0.11
    return2.yScale = display.contentHeight/return2.height*0.2
	return2.alpha = 0
	transition.to(return2,{time=1000,alpha=1,delay=1500})
    group_images:insert(return2)
	sceneGroup:insert(return2)
	return2:addEventListener("touch", return2Handler)
end


-- show()
function scene:show( event )
	print("show in : ")
    local sceneGroup = self.view
    local phase = event.phase
	local channel = nil
	self.channel = channel
local option = {
	loops=0
}
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

	timer.performWithDelay(1, function()
	channel = sfx:play("victory",option)
	end )
	
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )
print("hide in : ")
    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene