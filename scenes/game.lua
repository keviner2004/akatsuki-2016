local composer = require( "composer" )

local scene = composer.newScene()
local sfx = require ("sfx")
local Sprite = require("Sprite")
local gameConfig = require("gameConfig")
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------


local MQTT = require("mqtt_library")
    function callback(
        topic,    -- string
        payload)  -- string
        print("omg")
        print("mqtt_test:callback(): " ,topic ,": " ,payload)
        scene:attack_ha_ha()
    end
    local mqtt_client = MQTT.client.create("hahafighter21922.cloudapp.net", "1883", callback)
    local function tick()
      if mqtt_client.connected then
        local error_message = mqtt_client:handler()
        
        -- if (error_message == nil) then
        --     mqtt_client:publish(args.topic_p, "*** Lua test message ***")
        --     -- socket.sleep(1.0)  -- seconds
        -- end
        if (error_message ~= nil) then
          print("error_message:", error_message)
          mqtt_client:unsubscribe({ "test/1" })
          mqtt_client:destroy()
        end
      end
    end

    timer.performWithDelay( 500, tick, 0)

    local function uuid()
        math.randomseed( os.time() )
        local template ='xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
        return string.gsub(template, '[xy]', function (c)
            local v = (c == 'x') and math.random(0, 0xf) or math.random(8, 0xb)
            return string.format('%x', v)
        end)
    end

    mqtt_client:connect("User-kev-"..uuid())
    mqtt_client:subscribe({"test/1"})
    mqtt_client:publish("test/1", "*** Lua test message ***")
    mqtt_client:publish("test/1", "*** Lua test message ***")
    mqtt_client:publish("test/1", "*** Lua test message ***")

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
--[[
values
tempo line: left: contentWidth*1.5/10, contentHeight/10, right:contentWidth*9.5/10
--]]

-- create()
function scene:create( event )

    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
    local mid_x=display.contentWidth/2
    local mid_y=display.contentHeight/2
    local sfx_bgm_channel=nil
    --sfx:play("lev_1_bgm") -- bpm=200
    timer.performWithDelay(4000, function()
        sfx_bgm_channel=sfx:play("lev_1_bgm", {loops=-1})
        end)
    self.sfx_bgm_channel=sfx_bgm_channel
    local sfx_boss_channel=nil
    self.sfx_boss_channel=sfx_boss_channel
    local point_counter=0
    self.point_counter=point_counter
    local bottle_level=1
    self.bottle_level=bottle_level
    local combo_counter=0
    self.combo_counter=combo_counter
    local boss_hp=20
    self.boss_hp=boss_hp
    local tempo_count=0
    self.tempo_count=tempo_count
    local boss_generate=false
    self.boss_generate=boss_generate
    local text_combo=nil
    self.text_combo=text_combo

    -- load and set images
    local group_images = display.newGroup()
    local image_background_bottom = display.newImage("imgs/bg.png")
    image_background_bottom.xScale = display.contentWidth/image_background_bottom.width
    image_background_bottom.yScale = 36/57*display.contentHeight/image_background_bottom.height
    image_background_bottom.x=display.contentWidth/2
    image_background_bottom.y=display.contentHeight*39/57
    group_images:insert(image_background_bottom)

    local image_background = display.newImage("imgs/back1.png")
    image_background.xScale= display.contentWidth/image_background.width
    image_background.yScale= 21/57*display.contentHeight/image_background.height
    image_background.x=display.contentWidth/2
    image_background.y=display.contentHeight*105/570
    group_images:insert(image_background)

    local image_blackboard = display.newImage("imgs/blackboard.png")
    image_blackboard.xScale = 280/770*display.contentWidth/image_blackboard.width
    image_blackboard.yScale = 160/570*display.contentHeight/image_blackboard.height
    image_blackboard.x=display.contentWidth*320/770
    image_blackboard.y=display.contentHeight*480/570
    group_images:insert(image_blackboard)
    local text_name=display.newText("FIGHTER", display.contentWidth*350/770, display.contentHeight*480/570, "PWChalk.ttf", 50*gameConfig.scaleFactor)
    group_images:insert(text_name)

    local image_character = Sprite.new("cha/cha1")
    image_character.xScale=250/770*display.contentWidth/image_character.width
    image_character.yScale=270/570*display.contentHeight/image_character.height
    image_character.x=display.contentWidth*140/770
    image_character.y=display.contentHeight*440/570
    group_images:insert(image_character)
    scene:bounce_up(image_character, 0)

    -- wait for new picture
    local image_fight_line = Sprite.new("LINE")
    image_fight_line.xScale = 0.25*display.contentHeight/image_fight_line.height
    image_fight_line.yScale = 0.25*display.contentHeight/image_fight_line.height
    image_fight_line.x=display.contentWidth*450/770
    image_fight_line.y=display.contentHeight/2
    group_images:insert(image_fight_line)

    local image_target = Sprite.new("Shape")
    image_target.xScale=0.25*display.contentHeight/image_target.height
    image_target.yScale=0.25*display.contentHeight/image_target.height
    image_target.x=display.contentWidth*300/770
    image_target.y=display.contentHeight/2
    group_images:insert(image_target)

    local image_bottle = Sprite.newAnimation({
        { name="bottle_level_1", frames={"bottle/gb1"}},
        { name="bottle_level_2", frames={"bottle/gb3"}}, -- naming error
        { name="bottle_level_3", frames={"bottle/gb2"}}, -- naming error
        { name="bottle_level_4", frames={"bottle/gb4"}},
        { name="bottle_level_5", frames={"bottle/gb5"}}
        })
    image_bottle:setSequence("bottle_level_1")
    image_bottle.xScale=270/570*display.contentHeight/image_bottle.height
    image_bottle.yScale=270/570*display.contentHeight/image_bottle.height
    image_bottle.rotation=-50
    image_bottle.x=display.contentWidth*650/770
    image_bottle.y=display.contentHeight*440/570
    self.image_bottle=image_bottle
    group_images:insert(image_bottle)

    local image_bad = Sprite.new("pop/pop")
    image_bad.xScale=0.25*display.contentHeight/image_bad.height
    image_bad.yScale=0.25*display.contentHeight/image_bad.height
    image_bad.x=display.contentWidth*150/770
    image_bad.y=display.contentHeight/2
    image_bad.alpha=0 -- initially invisible
    self.image_bad=image_bad
    group_images:insert(image_bad)

    local image_good = Sprite.new("pop/pop2")
    image_good.xScale=0.25*display.contentHeight/image_good.height
    image_good.yScale=0.25*display.contentHeight/image_good.height
    image_good.x=display.contentWidth*150/770
    image_good.y=display.contentHeight/2
    image_good.alpha=0 -- initially invisible
    self.image_good=image_good
    group_images:insert(image_good)

    local image_great = Sprite.new("pop/pop3")
    image_great.xScale=0.25*display.contentHeight/image_great.height
    image_great.yScale=0.25*display.contentHeight/image_great.height
    image_great.x=display.contentWidth*150/770
    image_great.y=display.contentHeight/2
    image_great.alpha=0 -- initially invisible
    self.image_great=image_great
    group_images:insert(image_great)

    local image_go = Sprite.new("go")
    image_go.xScale=0.3*gameConfig.scaleFactor
    image_go.yScale=0.3*gameConfig.scaleFactor
    image_go.x=display.contentWidth/2
    image_go.y=display.contentHeight/2
    group_images:insert(image_go)
    transition.to(image_go, {time=2000, delay=1000, alpha=1, xScale=2, yScale=2, onComplete=function()
        timer.performWithDelay(1000, function() if image_go~=nil then image_go:removeSelf() end end)
        end})

    local image_ready=Sprite.new("ready")
    sfx:play("countdown3");
    timer.performWithDelay(1000, function()
            sfx:play("countdown2")
        end)
    timer.performWithDelay(2000, function()
            sfx:play("countdown1")
        end)
    timer.performWithDelay(3000, function()
            sfx:play("start")
        end)
    image_ready.xScale=gameConfig.scaleFactor
    image_ready.yScale=gameConfig.scaleFactor
    image_ready.x=display.contentWidth/2
    image_ready.y=display.contentHeight/2
    group_images:insert(image_ready)
    transition.to(image_ready, {time=2000, delay=500, alpha=0, xScale=0.3, yScale=0.3, onComplete=function()
        if image_ready~=nil then
            image_ready:removeSelf()
        end
        end})

    sceneGroup:insert(group_images)

    
    -- groups
    local group_tempo_line = display.newGroup()
    local group_shine_border = display.newGroup()
    local group_face_circles = display.newGroup()
    group_face_circles.x=0
    group_face_circles.y=mid_y
    sceneGroup:insert(group_face_circles)
    self.group_face_circles=group_face_circles

    

    --group_tempo_line
    
    self.group_tempo_line=group_tempo_line

    local target_circle = display.newCircle(image_target.x,0,display.contentHeight/10)
    target_circle.fill = {1, 1, 0, 0} -- invisible target, use picture
    group_tempo_line:insert(target_circle)
    self.target_circle = target_circle

    group_tempo_line.x = 0
    group_tempo_line.y = mid_y
    sceneGroup:insert(group_tempo_line)

    --group_shine_border
    local border_thickness = display.contentWidth/100
    local up_rect=display.newRect(mid_x, 0, display.contentWidth, border_thickness)
    local left_rect=display.newRect(0, mid_y, border_thickness, display.contentHeight)
    local bottom_rect=display.newRect(mid_x, display.contentHeight, display.contentWidth, border_thickness)
    local right_rect=display.newRect(display.contentWidth, mid_y, border_thickness, display.contentHeight)

    -- default visible
    up_rect.fill={1,0,0,1}
    left_rect.fill={1,0,0,1}
    bottom_rect.fill={1,0,0,1}
    right_rect.fill={1,0,0,1}

    group_shine_border:insert(up_rect)
    group_shine_border:insert(left_rect)
    group_shine_border:insert(bottom_rect)
    group_shine_border:insert(right_rect)
    self.group_shine_border=group_shine_border
    sceneGroup:insert(group_shine_border)

    -- touch rect
    local touch_rect=display.newRect(mid_x, mid_y, display.contentWidth, display.contentHeight)
    touch_rect.fill={0, 0, 0, 0}
    touch_rect.isHitTestable=true
    self.touch_rect=touch_rect

    local boss_summon=display.newRect(0,0, 100, 100)
    boss_summon.fill={1,1,1,0}
    boss_summon.isHitTestable=true
    self.boss_summon=boss_summon
end

-- enterframe listener
local function miss_face_circle(event)
    if scene.group_face_circles.numChildren~=0 then
        for i=1, scene.group_face_circles.numChildren do
            if scene.group_face_circles[i].x-scene.target_circle.x <0 then
                print("miss remove")
                if scene.group_face_circles[i]~=nil then
                    scene.group_face_circles[i]:removeSelf()
                end
                scene.combo_counter=0
                break
            end
        end
    end
end

local function generate_face_circle(event)
    --print(scene.tempo_count)
    if scene.point_counter<20 then
        local rand=math.random(1, 2)
        local rand2=math.random(1, 4)+1
        scene.tempo_count=scene.tempo_count+1
        if scene.tempo_count>= 36 then --100bpm
            scene.tempo_count=0
            if rand==1 then
                --generate
                scene:createFaceCircle(rand2, display.contentWidth)
            end
        end
    else
        if scene.boss_generate==false then
            scene:createFaceCircle(6, display.contentWidth)
            scene.boss_generate=true
            --if scene.sfx_bgm_channel~=nil then
                sfx:stop(scene.sfx_bgm_channel)
            --end
            scene.sfx_boss_channel = sfx:play("boss", {loops=-1})
        end
    end
end

-- bounce
function scene:bounce_up(char, i)
    if i>=100 then
        return nil
    end
    transition.to(char, {time=4000, xScale=1.5, yScale=1.5, onComplete=function()
        scene:bounce_down(char, i+1)
        end})
end

function scene:bounce_down(char, i)
    if i>=100 then
        return nil
    end
    transition.to(char, {time=4000, xScale=1.4, yScale=1.4, onComplete=function()
        scene:bounce_up(char, i+1)
        end})
end

-- create face circles
function scene:createLevel()
    local level = require("level.level1")

    for i = 1, #level.points do
        timer.performWithDelay(level.points[i].startTime, function()
            scene:createFaceCircle(level.points[i].type, level.points[i].init_x)
        end)
    end
end

function scene:createFaceCircle(type, x)
    local face_circle =nil
    if type==1 then
        face_circle=Sprite.new("c1")
    elseif type==2 then 
        face_circle=Sprite.new("c2")
    elseif type==3 then 
        face_circle=Sprite.new("c3")
    elseif type==4 then 
        face_circle=Sprite.new("c4")
    elseif type==5 then 
        face_circle=Sprite.new("c4") -- no long face
    elseif type==6 then
        print("ghost!")
        face_circle=Sprite.new("GHOST")
    end
    face_circle.type = type
    face_circle.xScale=0.25*display.contentHeight/face_circle.height
    face_circle.yScale=0.25*display.contentHeight/face_circle.height
    if type==6 then
        face_circle.xScale=0.8*display.contentHeight/face_circle.height
        face_circle.yScale=0.8*display.contentHeight/face_circle.height
    end
    face_circle.x=x+display.contentWidth*8/10
    face_circle.y=0
    local face_target_distance=x-scene.target_circle.x+display.contentWidth*8/10
    local face_circle_speed=3

    scene.group_face_circles:insert(face_circle)
    if type==6 then
        transition.to(face_circle, {time=face_target_distance*face_circle_speed, x=display.contentWidth/2, y=(0)})
    else
        transition.to(face_circle, {time=face_target_distance*face_circle_speed, x=display.contentWidth*1.5/10-10, y=(0)})
    end
    return tempCircle
end

-- bottle update
function scene:bottle_update()
    if scene.point_counter>2 and scene.bottle_level==1 then
        scene.bottle_level=2
        scene.image_bottle:setSequence("bottle_level_2")
    elseif scene.point_counter>5 and scene.bottle_level==2 then
        scene.bottle_level=3
        scene.image_bottle:setSequence("bottle_level_3")
    elseif scene.point_counter>10 and scene.bottle_level==3 then
        scene.bottle_level=4
        scene.image_bottle:setSequence("bottle_level_4")
    elseif scene.point_counter>15 and scene.bottle_level==4 then
        scene.bottle_level=5
        scene.image_bottle:setSequence("bottle_level_5")
    end
end

-- combo update
function scene:combo_update()
    
    if scene.combo_counter>0 then
        --show combo
        if scene.text_combo~=nil then
            scene.text_combo:removeSelf();
            scene.text_combo=nil
        end
        scene.text_combo=display.newText(tostring(scene.combo_counter), display.contentWidth*70/770, display.contentHeight/3, "BADABB__.TTF", 160*gameConfig.scaleFactor)
        local text=display.newText("combo", display.contentWidth*170/770, display.contentHeight/3, "BADABB__.TTF", 80*gameConfig.scaleFactor)
        scene.text_combo.fill={127/255, 213/255, 237/255, 230/255}
        text.fill={127/255, 213/255, 237/255, 230/255}

        scene.text_combo.combo_counter=scene.combo_counter

        transition.to(scene.text_combo, {time=3000, alpha=0, onComplete=function()
            if scene.text_combo~=nil and scene.text_combo.combo_counter~=scene.combo_counter then
                scene.text_combo:removeSelf()
                scene.text_combo=nil
            end
        end})
        transition.to(text, {time=3000, alpha=0, onComplete=function()
            if text~=nil then
                text:removeSelf()
                text=nil
            end
        end})
    elseif scene.combo_counter==0 then
        --remove combo
        if scene.text_combo~=nil and scene.text_combo.combo_counter~=scene.combo_counter then
            scene.text_combo:removeSelf()
            scene.text_combo=nil
        end
    end
end

-- boss jitter
function scene:boss_jitter_left(boss, times)
    if times>=10 then
        return nil
    end
    transition.to(boss, {rotation=-10, time=50, onComplete=function(events)
        scene:boss_jitter_right(boss, times+1)
        end})
end

function scene:boss_jitter_right(boss, times)
    if times>=10 then
        return nil
    end
    transition.to(boss, {rotation=10, time=50, onComplete=function(events)
        scene:boss_jitter_left(boss, times+1)
        end})
end

-- attack by ha ha
function scene:attack_ha_ha()
    local distance_list={}
    
        if scene.group_face_circles.numChildren~=0 then
            for i=1, scene.group_face_circles.numChildren do
                distance_list[#distance_list+1]=scene.group_face_circles[i].x-scene.target_circle.x
            end

            -- find index with min distance
            local min=99999
            local min_index=0
            for i=1, #distance_list do
                if distance_list[i]<=min then
                    min=distance_list[i]
                    min_index=i
                end
            end

            local showtime=300
            if scene.group_face_circles[min_index].type==6 then
                scene:boss_jitter_left(scene.group_face_circles[min_index], 0)
                scene:HaHa(scene.group_face_circles[min_index].x, scene.group_face_circles[min_index].y)
                transition.to(scene.group_face_circles[min_index], {rotation=-10, time=50})
                scene.boss_hp=scene.boss_hp-1
                if scene.boss_hp<=0 then
                    transition.to(scene.group_face_circles[min_index], {time=1000, xScale=3.5, yScale=3.5, alpha=0, onComplete=function()
                        if scene.group_face_circles[min_index]~=nil then
                            scene.group_face_circles[min_index]:removeSelf()
                        end
                        end})
                    --scene.group_face_circles[min_index]:removeSelf()
                    --win
                    print("victory")
                    sfx:stop(scene.sfx_boss_channel)
                    gameConfig.point_counter=scene.point_counter
                    composer.showOverlay("scenes.gameover", {effect="fade", time=800})
                end
            else
                if min>=display.contentHeight/10 then
                    print("bad")
                    sfx:play("laugh_3")
                    --show bad
                    transition.to(scene.image_bad, {time=showtime, alpha=1, onComplete=function()
                        scene.image_bad.alpha=0
                        end})
                    scene.combo_counter=0
                elseif min>=display.contentHeight/50 and min<display.contentHeight/10 then
                    print("good")
                    sfx:play("laugh_1")
                    scene.point_counter=scene.point_counter+1
                    scene.bottle_update()
                    --show good
                    transition.to(scene.image_good, {time=showtime, alpha=1, onComplete=function()
                        scene.image_good.alpha=0
                        end})
                    --move to bag
                    
                    for i=1,6 do
                        local piece=nil
                        if scene.group_face_circles[min_index].type==1 then
                            piece=Sprite.new("p/pic1")
                        elseif scene.group_face_circles[min_index].type==2 then
                            piece=Sprite.new("p/pic2")
                        elseif scene.group_face_circles[min_index].type==3 then
                            piece=Sprite.new("p/pic3")
                        elseif scene.group_face_circles[min_index].type==4 then
                            piece=Sprite.new("p/pic4")
                        else
                            piece=Sprite.new("p/pic4")
                        end
                        piece.x=scene.group_face_circles[min_index].x
                        piece.y=scene.group_face_circles[min_index].y + display.contentHeight/2
                        piece.rotation=math.random(360)
                        transition.to(piece, {time=(math.random(500)+500), x=display.contentWidth*7.1/8, y=display.contentHeight*8.2/10, onComplete=function()
                            if piece~=nil then
                                piece:removeSelf()
                            end
                        end})
                    end 
                    if scene.group_face_circles[min_index]~=nil then
                        scene.group_face_circles[min_index]:removeSelf()
                    end
                    scene.combo_counter=scene.combo_counter+1
                    scene.combo_update()
                elseif min<display.contentHeight/50 and min>=0 then
                    print("great")
                    sfx:play("laugh_2")
                    scene.point_counter=scene.point_counter+2
                    scene.bottle_update()
                    --show great
                    transition.to(scene.image_great, {time=showtime, alpha=1, onComplete=function()
                        scene.image_great.alpha=0
                        end})
                    --move to bag
                    for i=1,6 do
                        local piece=nil
                        if scene.group_face_circles[min_index].type==1 then
                            piece=Sprite.new("p/pic1")
                        elseif scene.group_face_circles[min_index].type==2 then
                            piece=Sprite.new("p/pic2")
                        elseif scene.group_face_circles[min_index].type==3 then
                            piece=Sprite.new("p/pic3")
                        elseif scene.group_face_circles[min_index].type==4 then
                            piece=Sprite.new("p/pic4")
                        else
                            piece=Sprite.new("p/pic4")
                        end
                        piece.x=scene.group_face_circles[min_index].x
                        piece.y=scene.group_face_circles[min_index].y + display.contentHeight/2
                        piece.rotation=math.random(360)
                        transition.to(piece, {time=(math.random(500)+500), x=display.contentWidth*7.1/8, y=display.contentHeight*8.2/10, onComplete=function()
                            if piece~=nil then
                                piece:removeSelf()
                            end
                        end})
                    end
                    if scene.group_face_circles[min_index]~=nil then
                        scene.group_face_circles[min_index]:removeSelf()
                    end
                    scene.combo_counter=scene.combo_counter+1 
                    scene.combo_update()
                end
            end -- end if not type==6
        end
end -- function attack ha ha

function scene:HaHa(orgx, orgy)
    local ha1=display.newText({
        text = "Ha", 
        x = orgx, y = orgy+display.contentHeight/2, 
        font="BADABB__.TTF",
        fontSize = 80*gameConfig.scaleFactor
    })
    local ha2=display.newText({
        text = "Ha", 
        x = orgx, y = orgy+display.contentHeight/2, 
        font="BADABB__.TTF",
        fontSize = 80*gameConfig.scaleFactor
    })
    local ha3=display.newText({
        text = "Ha", 
        x = orgx, y = orgy+display.contentHeight/2, 
        font="BADABB__.TTF",
        fontSize = 80*gameConfig.scaleFactor
    })
    scene.view:insert(ha1)
    scene.view:insert(ha2)
    scene.view:insert(ha3)
    transition.to(ha1, {time=math.random(1,300)+1000, x=orgx+math.random(1,500)-250, y=orgy-math.random(1,250), alpha=0.3, onComplete=function()
        if ha1~=nil then
            ha1:removeSelf()
        end 
    end})
    transition.to(ha2, {time=math.random(1,300)+1000, x=orgx+math.random(1,500)-250, y=orgy-math.random(1,250), alpha=0.3, onComplete=function()
        if ha2~=nil then
            ha2:removeSelf()
        end
    end})
    transition.to(ha3, {time=math.random(1,300)+1000, x=orgx+math.random(1,500)-250, y=orgy-math.random(1,250), alpha=0.3, onComplete=function()
        if ha3~=nil then
            ha3:removeSelf()
        end
    end})
end

-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    local mid_x=display.contentWidth/2
    local mid_y=display.contentHeight/2
    
    local face_circle=self.face_circle
    local target_circle=self.target_circle

    local group_shine_border=self.group_shine_border

    local touch_rect=self.touch_rect
    local boss_summon=self.boss_summon
    scene.view:insert(touch_rect)
    scene.view:insert(boss_summon)

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
        
        -- 160bpm should be 60000/160=375ms, but blink will do twice -> 600, slow down 1200
        timer.performWithDelay(4000, function()
        transition.blink(group_shine_border, {time = 670 }) 
        end)
        

        -- touch listener
        function touch_rect:touch(event)
            local distance_list={}
            if event.phase=="began" then
                if scene.group_face_circles.numChildren~=0 then
                    for i=1, scene.group_face_circles.numChildren do
                        distance_list[#distance_list+1]=scene.group_face_circles[i].x-scene.target_circle.x
                    end

                    -- find index with min distance
                    local min=99999
                    local min_index=0
                    for i=1, #distance_list do
                        if distance_list[i]<=min then
                            min=distance_list[i]
                            min_index=i
                        end
                    end

                    local showtime=300
                    if scene.group_face_circles[min_index].type==6 then
                        scene:boss_jitter_left(scene.group_face_circles[min_index], 0)
                        scene:HaHa(scene.group_face_circles[min_index].x, scene.group_face_circles[min_index].y)
                        transition.to(scene.group_face_circles[min_index], {rotation=-10, time=50})
                        scene.boss_hp=scene.boss_hp-1
                        if scene.boss_hp<=0 then
                            transition.to(scene.group_face_circles[min_index], {time=1000, xScale=3.5, yScale=3.5, alpha=0, onComplete=function()
                                if scene.group_face_circles[min_index]~=nil then
                                    scene.group_face_circles[min_index]:removeSelf()
                                end
                            end})
                            --win
                            print("victory")
                            gameConfig.point_counter=scene.point_counter
                            composer.showOverlay("scenes.gameover", {effect="fade", time=800})
                            
                                sfx:stop(sfx_boss_channel)
                            
                        end
                    else
                        if min>=display.contentHeight/10 then
                            print("bad")
                            sfx:play("laugh_3")
                            --show bad
                            transition.to(scene.image_bad, {time=showtime, alpha=1, onComplete=function()
                                scene.image_bad.alpha=0
                                end})
                            scene.combo_counter=0
                        elseif min>=display.contentHeight/50 and min<display.contentHeight/10 then
                            print("good")
                            sfx:play("laugh_1")
                            scene.point_counter=scene.point_counter+1
                            scene.bottle_update()
                            --show good
                            transition.to(scene.image_good, {time=showtime, alpha=1, onComplete=function()
                                scene.image_good.alpha=0
                                end})
                            --move to bag
                            
                            for i=1,6 do
                                local piece=nil
                                if scene.group_face_circles[min_index].type==1 then
                                    piece=Sprite.new("p/pic1")
                                elseif scene.group_face_circles[min_index].type==2 then
                                    piece=Sprite.new("p/pic2")
                                elseif scene.group_face_circles[min_index].type==3 then
                                    piece=Sprite.new("p/pic3")
                                elseif scene.group_face_circles[min_index].type==4 then
                                    piece=Sprite.new("p/pic4")
                                else
                                    piece=Sprite.new("p/pic4")
                                end
                                piece.x=scene.group_face_circles[min_index].x
                                piece.y=scene.group_face_circles[min_index].y + display.contentHeight/2
                                piece.rotation=math.random(360)
                                transition.to(piece, {time=(math.random(500)+500), x=display.contentWidth*7.1/8, y=display.contentHeight*8.2/10, onComplete=function()
                                    if piece~=nil then
                                        piece:removeSelf()
                                    end
                                end})
                            end 
                            if scene.group_face_circles[min_index]~=nil then
                                scene.group_face_circles[min_index]:removeSelf()
                            end
                            scene.combo_counter=scene.combo_counter+1
                            scene.combo_update()
                        elseif min<display.contentHeight/50 and min>=0 then
                            print("great")
                            sfx:play("laugh_2")
                            scene.point_counter=scene.point_counter+2
                            scene.bottle_update()
                            --show great
                            transition.to(scene.image_great, {time=showtime, alpha=1, onComplete=function()
                                scene.image_great.alpha=0
                                end})
                            --move to bag
                            for i=1,6 do
                                local piece=nil
                                if scene.group_face_circles[min_index].type==1 then
                                    piece=Sprite.new("p/pic1")
                                elseif scene.group_face_circles[min_index].type==2 then
                                    piece=Sprite.new("p/pic2")
                                elseif scene.group_face_circles[min_index].type==3 then
                                    piece=Sprite.new("p/pic3")
                                elseif scene.group_face_circles[min_index].type==4 then
                                    piece=Sprite.new("p/pic4")
                                else
                                    piece=Sprite.new("p/pic4")
                                end
                                piece.x=scene.group_face_circles[min_index].x
                                piece.y=scene.group_face_circles[min_index].y + display.contentHeight/2
                                piece.rotation=math.random(360)
                                transition.to(piece, {time=(math.random(500)+500), x=display.contentWidth*7.1/8, y=display.contentHeight*8.2/10, onComplete=function()
                                    if piece~=nil then
                                        piece:removeSelf()
                                    end
                                end})
                            end
                            if scene.group_face_circles[min_index]~=nil then
                                scene.group_face_circles[min_index]:removeSelf()
                            end
                            scene.combo_counter=scene.combo_counter+1 
                            scene.combo_update()
                        end
                    end -- end if not type==6
                end
            end -- phase== began
            return true
        end -- function touch

        function boss_summon:touch(event)
            if event.phase=="began" then
                scene.point_counter=30
            end
            return true
        end
        
        touch_rect:addEventListener( "touch", touch_rect)
        boss_summon:addEventListener("touch", boss_summon)
        Runtime:addEventListener("enterFrame", miss_face_circle)
        Runtime:addEventListener("enterFrame", generate_face_circle)

        --self.createLevel()

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    local touch_rect=self.touch_rect
    local boss_summon=self.boss_summon

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
        touch_rect:removeEventListener("touch", touch_rect)
        boss_summon:removeEventListener("touch", boss_summon)
        Runtime:removeEventListener("enterFrame", miss_face_circle)
        Runtime:removeEventListener("enterFrame", generate_face_circle)


    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end




-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -----------------------------------------------------------------------------------

return scene