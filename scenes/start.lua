local composer = require( "composer" )
local Sprite = require("Sprite")
local gameConfig = require("gameConfig")
local RPGPanel1 = require("ui.rpg.Panel1")
local RPGButton4 = require("ui.rpg.Button4")
local logger = require("logger")
local scene = composer.newScene()
local TAG = "scene.start"
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------




-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
    local panel = RPGPanel1.new(gameConfig.contentWidth/2, gameConfig.contentWidth/4)
    local button = RPGButton4.new(gameConfig.contentWidth/4, gameConfig.contentWidth/8)
    panel.x = gameConfig.contentCenterX
    panel.y = gameConfig.contentCenterY
    button.x = gameConfig.contentCenterX
    button.y = gameConfig.contentCenterY
    logger:info(TAG, "Panel %d, %d", panel.x, panel.y)
    sceneGroup:insert(panel)
end


-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene