local sfx = require ("sfx")
local composer = require( "composer" )
local widget = require( "widget" )
local scene = composer.newScene()
local gameConfig = require("gameConfig")
local Sprite = require("Sprite")
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

local function GoHandler( event )
	 if ( "ended" == event.phase ) then
        print( "Button was pressed and released" )
    end
	local options = {
    effect = "slideLeft",
    time = 200,
}
composer.gotoScene( "scenes.level_select", options )
end
-- create()
function scene:create( event )
	--BGM
	option = {	
	loops = -1,
	params = {play=1}
	}
	if gameConfig.TitleChannel == -1 then
	gameConfig.TitleChannel = sfx:play("title",option)
	end
	
	--display.setDefault("background",174/256, 214/256, 241/256)
    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
	local image_fight_line = display.newImage("imgs/bg start.png")
    image_fight_line.xScale = display.contentHeight*1.4/image_fight_line.height
    image_fight_line.yScale = display.contentHeight/image_fight_line.height
    image_fight_line.x=display.contentWidth/2
    image_fight_line.y=display.contentHeight/2
    --group_images:insert(image_fight_line)
	sceneGroup:insert(image_fight_line)
	-- Function to handle button events


end


-- show()
function scene:boundup( logo,i )
print("bound")
	
		--transition.to(logo, {time = 2000, xScale=1.1, yScale=1.1})
	if i >= 100 then
	return nil
	end
		transition.to(logo,{time=730,xScale=0.5, yScale=0.5, onComplete=function()
		scene:boundown( logo,i+1 )
		end})
		
	--scene:bound( logo,i+1 )
end
function scene:boundown( logo,i )
print("bound")
	
		--transition.to(logo, {time = 2000, xScale=1.1, yScale=1.1})
	if i >= 100 then
	return nil
	end
		transition.to(logo,{time=730, xScale=0.4,yScale=0.4,onComplete=function()
		scene:boundup( logo,i+1 )
		end})
		
		
	--scene:bound( logo,i+1 )
end
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
		local logo = display.newImage("imgs/logo.png")
    logo.xScale = display.contentHeight*0.9/logo.height
    logo.yScale = display.contentHeight*0.9/logo.height
    logo.x=display.contentWidth/2
    logo.y=display.contentHeight/2
	sceneGroup:insert(logo)
	i=0
	scene:boundup(logo,i)
	
	local Go = Sprite.new("go")
	Go.x=display.contentWidth*0.91
    Go.y=display.contentHeight*0.91
	sceneGroup:insert(Go)
	Go:addEventListener("touch", GoHandler)
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene