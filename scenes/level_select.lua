local composer = require( "composer" )
local Arrow = require ("r03942142.arrow_polygon")
local scene = composer.newScene()
local Sprite = require("Sprite")
local sfx = require ("sfx")
local gameConfig = require("gameConfig")
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------




-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
local function stage1Handler( event )
	if ( "ended" == event.phase ) then
        print( "Not yet!!" )
    end
		local options = {
    effect = "slideLeft",
    time = 200
}
composer.gotoScene( "scenes.map", options )
end
local function stage2Handler( event )
	if ( "ended" == event.phase ) then
        print( "Not yet!!" )
    end
--local alert = native.showAlert( "Oops!", "This stage hasn't opened yet", { "OK" }, onComplete )
end
local function image_targetHandler( event )
	if ( "began" == event.phase ) then
        print( "Not yet!!" )
		print ("c : "..scene.c)
		if scene.c == 0 then
			transition.to(scene.stage1,{time=800,alpha=0})
			transition.to(scene.stage2,{time=800,alpha=1})
			scene.c=1
	elseif scene.c == 1 then
			transition.to(scene.stage2,{time=800,alpha=0})
			transition.to(scene.stage1,{time=800,alpha=1})
			scene.c=0
    end
		
	end
	
end

local function image_target2Handler( event )
	if ( "began" == event.phase ) then
        print( "Not yet!!" )
		print ("c : "..scene.c)
		if scene.c == 0 then
			transition.to(scene.stage1,{time=1000,alpha=0})
			transition.to(scene.stage2,{time=1000,alpha=1})
			scene.c=1
	elseif scene.c == 1 then
			transition.to(scene.stage2,{time=1000,alpha=0})
			transition.to(scene.stage1,{time=1000,alpha=1})
			scene.c=0
    end
	
	end
	
end
--timer.performWithDelay( 100, image_targetHandler )
--timer.performWithDelay( 100, image_target2Handler )
function scene:create( event )
	
    local sceneGroup = self.view
	local halfW = display.contentWidth/2
	local halfH = display.contentHeight/2
	--ploygon
	local polygonW = 100
	local polygonH = 100
	local c=0
	scene.c = c
	local paint = { 0,0,0 }
    -- Code here runs when the scene is first created but has not yet appeared on screen
	--display.setDefault("background",1,1,1)
	local group_images = display.newGroup()
	local image_fight_line = display.newImage("imgs/bg3.png")
    image_fight_line.xScale = display.contentHeight*1.4/image_fight_line.height
    image_fight_line.yScale = display.contentHeight/image_fight_line.height
    image_fight_line.x=display.contentWidth/2
    image_fight_line.y=display.contentHeight/2
    group_images:insert(image_fight_line)
	sceneGroup:insert(image_fight_line)
	print("gameConfig.point_counter : "..gameConfig.point_counter)
	
	
	local stage2 = display.newImage("imgs/level/L2-1.png")
    stage2.xScale = display.contentHeight*1.15/stage2.height
    stage2.yScale = display.contentHeight*0.85/stage2.height
    stage2.x=display.contentWidth/2
    stage2.y=display.contentHeight/1.65
	group_images:insert(stage2)
	stage2.alpha = 0
	stage2:addEventListener("touch", stage2Handler)
	scene.stage2 = stage2
	
	local image_target = Sprite.new("Triangle Copy")
	image_target.x=display.contentWidth*0.9
    image_target.y=display.contentHeight/2
    group_images:insert(image_target)
	sceneGroup:insert(image_target)
	image_target:addEventListener("touch", image_targetHandler)
	
	local image_target2 = Sprite.new("Triangle")
	image_target2.x=display.contentWidth*0.1
    image_target2.y=display.contentHeight/2
    group_images:insert(image_target2)
	sceneGroup:insert(image_target2)
	image_target2:addEventListener("touch", image_target2Handler)
	
end


-- show()
function scene:show( event )
	local sceneGroup = self.view
    local phase = event.phase
		if event.params and not event.params.play then
			option = {	
	loops = -1,
	params={play=1}
	}
	gameConfig.TitleChannel = sfx:play("title",option)
		end
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
			if gameConfig.point_counter == 0 then
		local stage1 = display.newImage("imgs/level/L1-1.png")
		stage1.xScale = display.contentHeight*1.05/stage1.height
		stage1.yScale = display.contentHeight*0.85/stage1.height
		stage1.x=display.contentWidth/2
		stage1.y=display.contentHeight/1.65
		--group_images:insert(stage1)
		stage1.alpha = 1
		stage1:addEventListener("touch", stage1Handler)
		sceneGroup:insert(stage1)
		scene.stage1 = stage1
	else
		local stage1 = display.newImage("imgs/level/L1-2.png")
		stage1.xScale = display.contentHeight*1.05/stage1.height
		stage1.yScale = display.contentHeight*0.85/stage1.height
		stage1.x=display.contentWidth/2
		stage1.y=display.contentHeight/1.65
		--group_images:insert(stage1)
		stage1.alpha = 1
		stage1:addEventListener("touch", stage1Handler)
		sceneGroup:insert(stage1)
		scene.stage1 = stage1
	end
	--music
	--BGM
	option = {	
	loops = -1,
	params = {play=1}
	}
	if gameConfig.TitleChannel == -1 then
	gameConfig.TitleChannel = sfx:play("title",option)
	end
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene