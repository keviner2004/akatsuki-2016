local sfx = require("sfx")
local composer = require( "composer" )
local scene = composer.newScene()
local Sprite = require("Sprite")
local gameConfig = require("gameConfig")
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------




-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
local function myButtonHandler( event )
	if (event.phase == "began") then  
		print ("began")
		local options = {
    effect = "slideLeft",
    time = 200,
	params={play=0}
}
sfx:stop(gameConfig.TitleChannel)
gameConfig.TitleChannel = -1
composer.gotoScene( "scenes.game", options )
	end
end
local function questHandler( event )
	if (event.phase == "began") then  
		print ("began")
		local options = {
    effect = "slideRight",
    time = 200,
	params={play=0}
}
sfx:stop(gameConfig.TitleChannel)
gameConfig.TitleChannel = -1
composer.gotoScene( "scenes.level_select", options )
	end
end
function scene:create( event )
--BGM
	
    local sceneGroup = self.view
	local group_images = display.newGroup()
    -- Code here runs when the scene is first created but has not yet appeared on screen
	--get score from outside
	local tree_back = display.newImage("imgs/bg2.png")
    tree_back.xScale = display.contentHeight*1.4/tree_back.height
    tree_back.yScale = display.contentHeight/tree_back.height
    tree_back.x=display.contentWidth/2
    tree_back.y=display.contentHeight/2
	tree_back.alpha = 1
	--transition.to(tree_back, { time=1000, alpha=1})
	--transition.to( tree_back, { time=1500, alpha=0,delay=2500})
    group_images:insert(tree_back)
	sceneGroup:insert(tree_back)
	--return button
	local quest = Sprite.new("quest")
    quest.x=display.contentWidth*0.9
    quest.y=display.contentHeight*0.9
	quest.xScale = display.contentWidth/quest.width*0.15
    quest.yScale = display.contentHeight/quest.height*0.2
	quest.alpha=0
	transition.to(quest,{time=1000,alpha=1,delay=500})
	sceneGroup:insert(quest)
	quest:addEventListener("touch", questHandler)
	--t0
		
		local circleGroup = display.newGroup()
				
		--[[
		if gameConfig.point_counter > 10 then
			i=gameConfig.point_counter
		else
			i=5
		end
			local c = self:createCircle(sceneGroup,i)
			sceneGroup:insert(c)
			c.tag = "circle tag : "..i
			c.toScaleX = 2
			c.toScaleY = 2
			--self:setCircle(c,i)
			print (c.tag)
			--self:showCircle(c,i)
			--circleGroup:insert(c)
			sceneGroup:insert(c)
			print("circleGroup.numChildren"..circleGroup.numChildren)]]--
		--[[for i = 1, circleGroup.numChildren do
			if circleGroup[i].tag == "circle-"..i then
					self:setCircle(circleGroup,i)
			end
		end]]--
		
		--sceneGroup:insert(circleGroup)
		--[[self:setCircle(function()
			self:setCircle(function()
				self:setCircle(function()
				end)
			end)
		end)]]--
	--start button

	--button action
		-- Create the widget
	local polygonW = 1800
	local polygonH = 1600
	vertices = {  -polygonW/2,polygonH/2, polygonW/2,polygonH/2,  polygonW/2,-polygonH/2,-polygonW/2,-polygonH/2}
	local start = display.newCircle( display.contentWidth/2, display.contentHeight/2, 60 )
	start:setFillColor(1,0,0) 
	start.isVisible = false
	start.isHitTestable = true
--sceneGroup:insert(start)
-- Add a touch event handler to myButton
	start:addEventListener("touch", myButtonHandler)
	sceneGroup:insert(start)
end
function scene:showOneTree(tree,level)
	transition.to(tree, {time = 500, alpha = 1})
end
function scene:showTree(Tree_all,level)
	print ("Tree_all : ",Tree_all)
	print ("showTree levle+1 : ",Tree_all[level+1])
	transition.to(Tree_all[level], {time = 500, alpha = 1, onComplete=function()
		transition.to(Tree_all[level], {time = 500, alpha = 0, delay = 500})
		transition.to(Tree_all[level+1],{time = 500, alpha = 1, onComplete=function()
			transition.to(Tree_all[level+1], {time = 500, alpha = 0, delay = 500})
			transition.to(Tree_all[level+2],{time = 500, alpha = 1, onComplete=function()
				transition.to(Tree_all[level+2], {time = 500, alpha = 0, delay = 500})
					transition.to(Tree_all[level+3], {time = 500, alpha = 1, onComplete=function()
					transition.to(Tree_all[level+3],{time = 500, alpha = 0,delay = 500})
						transition.to(Tree_all[level+4], {time = 500, alpha = 1,onComplete=function()
						transition.to(Tree_all[level+4],{time = 500, alpha = 0, delay = 500})
							transition.to(Tree_all[level+5], {time = 500, alpha = 1,onComplete=function()
							transition.to(Tree_all[level+5],{time = 500, alpha = 0, delay = 500})
								transition.to(Tree_all[level+6], {time = 500, alpha = 1,onComplete=function()
								transition.to(Tree_all[level+6],{time = 500, alpha = 0, delay = 500})
									transition.to(Tree_all[level+7], {time = 500, alpha = 1, onComplete=function()
									transition.to(Tree_all[level+7],{time = 500, alpha = 0,  delay = 500})
										transition.to(Tree_all[level+8], {time = 500, alpha = 1,onComplete=function()
										transition.to(Tree_all[level+8],{time = 500, alpha = 0,delay = 500})
											transition.to(Tree_all[level+9],{time = 500, alpha = 1, onComplete=function()
											transition.to(Tree_all[level+9],{time = 500, alpha = 0, delay = 500})
												transition.to(Tree_all[level+10],{time = 500, alpha = 1, onComplete=function()
												transition.to(Tree_all[level+10],{time = 500, alpha = 0, delay = 500})
													transition.to(Tree_all[level+11],{time = 500, alpha = 1, onComplete=function()
													end})
											end})
								end})
									end})
								end})
							end})
						end})
					end})
				end})
			end})	
		end})
	end})
	if level == 12 then
	return nil
	end
end

-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase
	
local t0 = Sprite.new("t/tree0")
    t0.x=display.contentWidth/2
    t0.y=display.contentHeight/1.3
	t0.alpha=0
	sceneGroup:insert(t0)
	--group_images:insert(t0)
	--t1
	local t1 = Sprite.new("t/tree1")
    t1.x=display.contentWidth/2
    t1.y=display.contentHeight/1.3
	t1.alpha=0
	sceneGroup:insert(t1)
    --group_images:insert(t1)
	--t2
	local t2 = Sprite.new("t/tree2")
    t2.x=display.contentWidth/2
    t2.y=display.contentHeight/1.3
	t2.alpha=0
	sceneGroup:insert(t2)
    --group_images:insert(t2)
	--t2
	local t3 = Sprite.new("t/tree3")
    t3.x=display.contentWidth/2
    t3.y=display.contentHeight/1.33
	t3.alpha=0
	sceneGroup:insert(t3)
    --group_images:insert(t3)
	--t2
	local t4 = Sprite.new("t/tree4")
    t4.x=display.contentWidth/2
    t4.y=display.contentHeight/1.35
	t4.alpha=0
	sceneGroup:insert(t4)
    local t5 = Sprite.new("t/tree5")
    t5.x=display.contentWidth/2
    t5.y=display.contentHeight/1.38
	t5.alpha=0
	sceneGroup:insert(t5)
    --group_images:insert(t5)
	--t2
	local t6 = Sprite.new("t/tree6")
    t6.x=display.contentWidth/2
    t6.y=display.contentHeight/1.42
	t6.alpha=0
	sceneGroup:insert(t6)
    --group_images:insert(t6)
	--t7
	local t7 = Sprite.new("t/tree7")
    t7.x=display.contentWidth/2
    t7.y=display.contentHeight/1.48
	t7.alpha=0
	sceneGroup:insert(t7)
    --group_images:insert(t7)
	--t2
	local t8 = Sprite.new("t/tree8")
    t8.x=display.contentWidth/2
    t8.y=display.contentHeight/1.52
	t8.alpha=0
	sceneGroup:insert(t8)
    --group_images:insert(t8)
	--t2
	local t9 = Sprite.new("t/tree9")
    t9.x=display.contentWidth/2
    t9.y=display.contentHeight/1.56
	t9.alpha=0
	sceneGroup:insert(t9)
   -- group_images:insert(t9)
	
	local t10 = Sprite.new("treepop")
    t10.x=display.contentWidth/2
    t10.y=display.contentHeight/1.6
	t10.alpha=0
	sceneGroup:insert(t10)
	
	local t11 = Sprite.new("treepop2")
    t11.x=display.contentWidth/2
    t11.y=display.contentHeight/1.63
	print ("x : "..t11.x)
	print ("y : "..t11.y)
	t11.alpha=0
	sceneGroup:insert(t11)
	
	print ("point_counter : "..gameConfig.point_counter)
	local Tree_all = {t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11}
	
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
		if event.params and not event.params.play then
			option = {	
	loops = -1,
	params={play=1}
	}
	gameConfig.TitleChannel = sfx:play("title",option)
		end
		self.hidden = false
		--tree
	if gameConfig.point_counter == 0  then
		print("show one tree")
		self:showOneTree(t0,1)
	else 			
	--print ("Tree_all"..Tree_all[1])	
			self:showTree(Tree_all,1)
	end
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
		self.hidden = true
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end

function scene:createCircle(sceneGroup,level)
	local myCircle
	local point_x
	local point_y
	print("level"..level)
		point_x = display.contentWidth*0.5+math.random(-display.contentWidth*0.2,display.contentWidth*0.2)
		point_y = display.contentHeight*0.25+math.random(-display.contentHeight*0.1,display.contentHeight*0.1)
		myCircle = display.newCircle( point_x, point_y, 50 )
		myCircle.alpha=0
		myCircle:setFillColor(math.random(10,254)/256, math.random(10,254)/256, math.random(10,254)/256 )
		sceneGroup:insert(myCircle)
		scene:showCircle(sceneGroup,myCircle,level)
	return myCircle
end

function scene:setCircle(cg, level)
	if level == cg.numChildren then
		return nil
	end
	transition.scaleTo( cg[level], { xScale=2.0, yScale=2.0, time=2000, onComplete=function()
	scene:setCircle(cg[level+1],level+1)
	end} )
end

function scene:showCircle(sceneGroup,c,level)
		if level == 0 then
			return nil
		end
		print("show circle : "..level)
	if not self.hidden then
		transition.to(c, {time = 800, alpha = 1, onComplete=function()
		print("transition")
		if not self.hidden then
			self:createCircle(sceneGroup,level-1)
		end
		end})
	end
end
function scene:createTree(level)
	
	local polygonW = 60
	local polygonH = 50
	local tree_body_x = display.contentWidth*0.5
	local tree_body_y = display.contentHeight*0.85
	local left_down_x = -polygonW/2
	local down_y = -polygonH/2
	local right_down_x = polygonW/2
	local vertices = {  -polygonW/2,polygonH/2, polygonW/2,polygonH/2,  right_down_x,down_y,left_down_x,down_y }
	local tree_body = nil
	print (level)
	if level == 1 then
		--do nothing
	elseif level == 2 then
		tree_body_y = tree_body_y - 30
		polygonH = polygonH*1.1
		down_y = -polygonH/2
		vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2, right_down_x,down_y+display.contentHeight*0.1, left_down_x,down_y+display.contentHeight*0.1}
	elseif level == 3 then
		tree_body_y = tree_body_y - 30*2
		polygonH = polygonH*math.pow(1.1,2)
		down_y = -polygonH/2
		vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2, right_down_x,down_y+display.contentHeight*0.2, left_down_x,down_y+display.contentHeight*0.2 }
	elseif level == 4 then
		tree_body_y = tree_body_y - 30*3
		polygonH = polygonH*math.pow(1.1,3)
		down_y = -polygonH/2
		vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2, right_down_x,down_y+display.contentHeight*0.3, left_down_x,down_y+display.contentHeight*0.3 }
	else
		tree_body_y = tree_body_y - 30*4
		polygonH = polygonH*math.pow(1.1,4)
		down_y = -polygonH/2
		vertices = { -polygonW/2,polygonH/2, polygonW/2,polygonH/2, right_down_x,down_y+display.contentHeight*0.4, left_down_x,down_y+display.contentHeight*0.4 }
		--show tree
	end
	print("polygonW/2 : "..polygonW/2)
	print("polygonH/2 : "..polygonH/2)
	print("-polygonW/2 : "..-polygonW/2)
	print("polygonH/2 : "..polygonH/2)
	print("left_down_x : "..left_down_x)
	print("display.contentHeight*0.85 : "..display.contentHeight*0.85)
	print("right_down_x : "..right_down_x)
	print("display.contentHeight*0.85 : "..display.contentHeight*0.85)
	tree_color = { 165/256,42/256,42/256 }
	print("tree_body_x : "..tree_body_x)
	print("tree_body_y : "..tree_body_y)
	tree_body = display.newPolygon( tree_body_x, tree_body_y,vertices )
	tree_body.fill = tree_color
	
	return tree_body
end
function scene:deleteTree(t,level)
	t[level]:removeSelf()
	t[level] = nil
end

function scene:setTree(t,level, onFinish)
	print("create tree")
		transition.to(t, {time = 2000, alpha = 1, onComplete=function()
			self:deleteTree(t)
			--self.t = self:setTree(t,level)
			--if onFinish then
			--	onFinish()
			--end
		end})
	
end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene