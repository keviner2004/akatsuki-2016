local Logging = require("Logging")
local config = {}
--adjust below params if you want to publish
config.production = true
config.version = "0.1"
config.dbVersion = "0.1"
config.soundOn = true
config.logLevel = Logging.INFO
config.debugPhysics = false
config.contentWidth = display.contentWidth
config.contentHeight = display.contentHeight
config.contentCenterX = config.contentWidth/2
config.contentCenterY = config.contentHeight/2
config.gameover = false
config.point_counter=0
config.TitleChannel=-1


config.basicHeight = 640
config.imageSuffix = {
    ["@2x"] = 1.5,
    ["@3x"] = 2.5,
    ["@4x"] = 3
}
config.scaleFactor =  config.contentHeight / config.basicHeight

if config.production then
  config.debugFPS = false
else
  config.debugFPS = true
end

return config
